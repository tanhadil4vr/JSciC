/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic.utils;

import java.util.ArrayList;
import java.util.Collection;
import static org.apache.commons.math3.util.FastMath.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.edu.icm.jlargearrays.ComplexFloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LargeArrayArithmetics;
import pl.edu.icm.jlargearrays.LargeArrayType;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jscic.dataarrays.ComplexDataArray;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import static org.junit.Assert.assertEquals;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.edu.icm.jlargearrays.ConcurrencyUtils;

/**
 *
 * @author Piotr Wendykier, University of Warsaw, ICM
 */
@RunWith(value = Parameterized.class)
public class DataArrayArithmeticsTest
{

    private static final double DELTA_F = 1E-5;
    private static final double DELTA_D = 1E-13;

    @Parameterized.Parameters
    public static Collection<Object[]> getParameters()
    {
        final int[] threads = {1, 2, 8};

        final ArrayList<Object[]> parameters = new ArrayList<>();
        for (int i = 0; i < threads.length; i++) {
            parameters.add(new Object[]{threads[i]});
        }
        return parameters;
    }

    public DataArrayArithmeticsTest(int nthreads)
    {
        ConcurrencyUtils.setNumberOfThreads(nthreads);
        if (nthreads > 1) {
            ConcurrencyUtils.setConcurrentThreshold(1);
        }
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void testAddF()
    {
        int n = 10;

        //constant arrays
        DataArray da1 = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da1");
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_SHORT, n, 100, "da2");
        DataArray res = DataArrayArithmetics.addF(da1, da2);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(da1.getFloatElement(i)[0] + da2.getFloatElement(i)[0], res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 = veclen2 = 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, n), 1, "da1");
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.SHORT, n), 1, "da2");
        res = DataArrayArithmetics.addF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(da1.getFloatElement(i)[0] + da2.getFloatElement(i)[0], res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 = 1 && veclen2 > 1
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.SHORT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.addF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(da1.getFloatElement(i)[0] + da2.getFloatElement(i)[v], res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //veclen1 > 1 && veclen2 == 1
        res = DataArrayArithmetics.addF(da2, da1);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(da1.getFloatElement(i)[0] + da2.getFloatElement(i)[v], res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //veclen1 = veclen2 > 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.addF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(3, res.getVectorLength());

        ComplexDataArray da1c = (ComplexDataArray) da1;
        ComplexDataArray resc = (ComplexDataArray) res;

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(da1c.getFloatRealArray().getFloat(i * 3 + v) + da2.getFloatElement(i)[v], resc.getFloatRealArray().getFloat(i * 3 + v), DELTA_F);
                assertEquals(da1c.getFloatImaginaryArray().getFloat(i * 3 + v), resc.getFloatImaginaryArray().getFloat(i * 3 + v), DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        da1 = DataArray.create(td1, 1, "da1");

        ArrayList<Float> timeSeries2 = new ArrayList<>(2);
        timeSeries2.add(1.0f);
        timeSeries2.add(2.0f);
        ArrayList<LargeArray> dataSeries2 = new ArrayList<>(2);
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        TimeData td2 = new TimeData(timeSeries2, dataSeries2, timeSeries2.get(0));
        da2 = DataArray.create(td2, 2, "da2");

        res = DataArrayArithmetics.addF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        td1 = td1.convertToFloat();
        td2 = td2.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td1.getValue(time);
            LargeArray b = td2.getValue(time);
            LargeArray c = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(a.getFloat(i) + b.getFloat(i * 2 + v), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testAddD()
    {
        int n = 10;

        //constant arrays
        DataArray da1 = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da1");
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_INT, n, 100, "da2");
        DataArray res = DataArrayArithmetics.addD(da1, da2);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(da1.getDoubleElement(i)[0] + da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 = veclen2 = 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, n), 1, "da1");
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.INT, n), 1, "da2");
        res = DataArrayArithmetics.addD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(da1.getDoubleElement(i)[0] + da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 = 1 && veclen2 > 1
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.INT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.addD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(da1.getDoubleElement(i)[0] + da2.getDoubleElement(i)[v], res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //veclen1 > 1 && veclen2 == 1
        res = DataArrayArithmetics.addD(da2, da1);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(da1.getDoubleElement(i)[0] + da2.getDoubleElement(i)[v], res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //veclen1 = veclen2 > 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.addD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(3, res.getVectorLength());

        ComplexDataArray da1c = (ComplexDataArray) da1;
        ComplexDataArray resc = (ComplexDataArray) res;

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(da1c.getFloatRealArray().getFloat(i * 3 + v) + da2.getFloatElement(i)[v], resc.getFloatRealArray().getFloat(i * 3 + v), DELTA_F);
                assertEquals(da1c.getFloatImaginaryArray().getFloat(i * 3 + v), resc.getFloatImaginaryArray().getFloat(i * 3 + v), DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        da1 = DataArray.create(td1, 1, "da1");

        ArrayList<Float> timeSeries2 = new ArrayList<>(2);
        timeSeries2.add(1.0f);
        timeSeries2.add(2.0f);
        ArrayList<LargeArray> dataSeries2 = new ArrayList<>(2);
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        TimeData td2 = new TimeData(timeSeries2, dataSeries2, timeSeries2.get(0));
        da2 = DataArray.create(td2, 2, "da2");

        res = DataArrayArithmetics.addD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        td1 = td1.convertToDouble();
        td2 = td2.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td1.getValue(time);
            LargeArray b = td2.getValue(time);
            LargeArray c = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(a.getDouble(i) + b.getDouble(i * 2 + v), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }
    }

    @Test
    public void testDiffF()
    {
        int n = 10;

        //constant arrays
        DataArray da1 = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da1");
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_SHORT, n, 100, "da2");
        DataArray res = DataArrayArithmetics.diffF(da1, da2);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(da1.getFloatElement(i)[0] - da2.getFloatElement(i)[0], res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 = veclen2 = 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, n), 1, "da1");
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.SHORT, n), 1, "da2");
        res = DataArrayArithmetics.diffF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(da1.getFloatElement(i)[0] - da2.getFloatElement(i)[0], res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 = 1 && veclen2 > 1
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.SHORT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.diffF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(da1.getFloatElement(i)[0] - da2.getFloatElement(i)[v], res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //veclen1 > 1 && veclen2 == 1
        res = DataArrayArithmetics.diffF(da2, da1);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(da2.getFloatElement(i)[v] - da1.getFloatElement(i)[0], res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //veclen1 = veclen2 > 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.diffF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(3, res.getVectorLength());

        ComplexDataArray da1c = (ComplexDataArray) da1;
        ComplexDataArray resc = (ComplexDataArray) res;

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(da1c.getFloatRealArray().getFloat(i * 3 + v) - da2.getFloatElement(i)[v], resc.getFloatRealArray().getFloat(i * 3 + v), DELTA_F);
                assertEquals(da1c.getFloatImaginaryArray().getFloat(i * 3 + v), resc.getFloatImaginaryArray().getFloat(i * 3 + v), DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        da1 = DataArray.create(td1, 1, "da1");

        ArrayList<Float> timeSeries2 = new ArrayList<>(2);
        timeSeries2.add(1.0f);
        timeSeries2.add(2.0f);
        ArrayList<LargeArray> dataSeries2 = new ArrayList<>(2);
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        TimeData td2 = new TimeData(timeSeries2, dataSeries2, timeSeries2.get(0));
        da2 = DataArray.create(td2, 2, "da2");

        res = DataArrayArithmetics.diffF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        td1 = td1.convertToFloat();
        td2 = td2.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td1.getValue(time);
            LargeArray b = td2.getValue(time);
            LargeArray c = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(a.getFloat(i) - b.getFloat(i * 2 + v), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testDiffD()
    {
        int n = 10;

        //constant arrays
        DataArray da1 = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da1");
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_INT, n, 100, "da2");
        DataArray res = DataArrayArithmetics.diffD(da1, da2);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(da1.getDoubleElement(i)[0] - da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 = veclen2 = 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, n), 1, "da1");
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.INT, n), 1, "da2");
        res = DataArrayArithmetics.diffD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(da1.getDoubleElement(i)[0] - da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 = 1 && veclen2 > 1
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.INT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.diffD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(da1.getDoubleElement(i)[0] - da2.getDoubleElement(i)[v], res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //veclen1 > 1 && veclen2 == 1
        res = DataArrayArithmetics.diffD(da2, da1);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(da2.getDoubleElement(i)[v] - da1.getDoubleElement(i)[0], res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //veclen1 = veclen2 > 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.diffD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(3, res.getVectorLength());

        ComplexDataArray da1c = (ComplexDataArray) da1;
        ComplexDataArray resc = (ComplexDataArray) res;

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(da1c.getFloatRealArray().getFloat(i * 3 + v) - da2.getFloatElement(i)[v], resc.getFloatRealArray().getFloat(i * 3 + v), DELTA_F);
                assertEquals(da1c.getFloatImaginaryArray().getFloat(i * 3 + v), resc.getFloatImaginaryArray().getFloat(i * 3 + v), DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        da1 = DataArray.create(td1, 1, "da1");

        ArrayList<Float> timeSeries2 = new ArrayList<>(2);
        timeSeries2.add(1.0f);
        timeSeries2.add(2.0f);
        ArrayList<LargeArray> dataSeries2 = new ArrayList<>(2);
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        TimeData td2 = new TimeData(timeSeries2, dataSeries2, timeSeries2.get(0));
        da2 = DataArray.create(td2, 2, "da2");

        res = DataArrayArithmetics.diffD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        td1 = td1.convertToDouble();
        td2 = td2.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td1.getValue(time);
            LargeArray b = td2.getValue(time);
            LargeArray c = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(a.getDouble(i) - b.getDouble(i * 2 + v), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }
    }

    @Test
    public void testMultF()
    {
        int n = 10;

        //constant arrays
        DataArray da1 = DataArray.createConstant(DataArrayType.FIELD_DATA_FLOAT, n, 1, "da1");
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_DOUBLE, n, 100, "da2");
        DataArray res = DataArrayArithmetics.multF(da1, da2);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(da1.getFloatElement(i)[0] * da2.getFloatElement(i)[0], res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 = veclen2 = 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1");
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2");
        res = DataArrayArithmetics.multF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(da1.getFloatElement(i)[0] * da2.getFloatElement(i)[0], res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 = 1 && veclen2 > 1
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da2");
        res = DataArrayArithmetics.multF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(da1.getFloatElement(i)[0] * da2.getFloatElement(i)[v], res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //veclen1 > 1 && veclen2 == 1
        res = DataArrayArithmetics.multF(da2, da1);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(da2.getFloatElement(i)[v] * da1.getFloatElement(i)[0], res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //veclen1 = veclen2 > 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.multF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(1, res.getVectorLength());

        ComplexDataArray da1c = (ComplexDataArray) da1;
        ComplexDataArray resc = (ComplexDataArray) res;
        ComplexFloatLargeArray ca1 = da1c.getRawArray();
        ComplexFloatLargeArray resca = resc.getRawArray();

        for (int i = 0; i < n; i++) {
            float[] dot = new float[2];
            for (int v = 0; v < 3; v++) {
                float[] elem_a = ca1.getComplexFloat(i * 3 + v);
                float[] elem_b = new float[]{da2.getFloatElement(i)[v], 0};
                dot = LargeArrayArithmetics.complexAdd(dot, LargeArrayArithmetics.complexMult(elem_a, elem_b));
            }
            Assert.assertArrayEquals(dot, resca.getComplexFloat(i), (float) DELTA_F);
        }

        //time steps > 1
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        da1 = DataArray.create(td1, 1, "da1");

        ArrayList<Float> timeSeries2 = new ArrayList<>(2);
        timeSeries2.add(1.0f);
        timeSeries2.add(2.0f);
        ArrayList<LargeArray> dataSeries2 = new ArrayList<>(2);
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 2 * n));
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 2 * n));
        TimeData td2 = new TimeData(timeSeries2, dataSeries2, timeSeries2.get(0));
        da2 = DataArray.create(td2, 2, "da2");

        res = DataArrayArithmetics.multF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        td1 = td1.convertToFloat();
        td2 = td2.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td1.getValue(time);
            LargeArray b = td2.getValue(time);
            LargeArray c = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(a.getFloat(i) * b.getFloat(i * 2 + v), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testMultD()
    {
        int n = 10;

        //constant arrays
        DataArray da1 = DataArray.createConstant(DataArrayType.FIELD_DATA_FLOAT, n, 1, "da1");
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_DOUBLE, n, 100, "da2");
        DataArray res = DataArrayArithmetics.multD(da1, da2);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(da1.getDoubleElement(i)[0] * da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 = veclen2 = 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, n), 1, "da1");
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da2");
        res = DataArrayArithmetics.multD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(da1.getDoubleElement(i)[0] * da2.getDoubleElement(i)[0], res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 = 1 && veclen2 > 1
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da2");
        res = DataArrayArithmetics.multD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(da1.getDoubleElement(i)[0] * da2.getDoubleElement(i)[v], res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //veclen1 > 1 && veclen2 == 1
        res = DataArrayArithmetics.multD(da2, da1);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(da2.getDoubleElement(i)[v] * da1.getDoubleElement(i)[0], res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //veclen1 = veclen2 > 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.multD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(1, res.getVectorLength());

        ComplexDataArray da1c = (ComplexDataArray) da1;
        ComplexDataArray resc = (ComplexDataArray) res;

        ComplexFloatLargeArray ca1 = da1c.getRawArray();
        ComplexFloatLargeArray resca = resc.getRawArray();

        for (int i = 0; i < n; i++) {
            float[] dot = new float[2];
            for (int v = 0; v < 3; v++) {
                float[] elem_a = ca1.getComplexFloat(i * 3 + v);
                float[] elem_b = new float[]{da2.getFloatElement(i)[v], 0};
                dot = LargeArrayArithmetics.complexAdd(dot, LargeArrayArithmetics.complexMult(elem_a, elem_b));
            }
            Assert.assertArrayEquals(dot, resca.getComplexFloat(i), (float) DELTA_F);
        }

        //time steps > 1
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        da1 = DataArray.create(td1, 1, "da1");

        ArrayList<Float> timeSeries2 = new ArrayList<>(2);
        timeSeries2.add(1.0f);
        timeSeries2.add(2.0f);
        ArrayList<LargeArray> dataSeries2 = new ArrayList<>(2);
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 2 * n));
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.FLOAT, 2 * n));
        TimeData td2 = new TimeData(timeSeries2, dataSeries2, timeSeries2.get(0));
        da2 = DataArray.create(td2, 2, "da2");

        res = DataArrayArithmetics.multD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        td1 = td1.convertToDouble();
        td2 = td2.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td1.getValue(time);
            LargeArray b = td2.getValue(time);
            LargeArray c = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(a.getDouble(i) * b.getDouble(i * 2 + v), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }
    }

    @Test
    public void testDivF()
    {
        int n = 10;

        //constant arrays
        DataArray da1 = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da1");
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_SHORT, n, 100, "da2");
        DataArray res = DataArrayArithmetics.divF(da1, da2);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(da1.getFloatElement(i)[0] / da2.getFloatElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 = veclen2 = 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, n), 1, "da1");
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.SHORT, n), 1, "da2");
        res = DataArrayArithmetics.divF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(da1.getFloatElement(i)[0] / da2.getFloatElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 = 1 && veclen2 > 1
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.SHORT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.divF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(da1.getFloatElement(i)[0] / da2.getFloatElement(i)[v], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //veclen1 > 1 && veclen2 == 1
        res = DataArrayArithmetics.divF(da2, da1);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(da2.getFloatElement(i)[v] / da1.getFloatElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //veclen1 = veclen2 > 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.divF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(3, res.getVectorLength());

        ComplexDataArray da1c = (ComplexDataArray) da1;
        ComplexDataArray resc = (ComplexDataArray) res;
        ComplexFloatLargeArray ca1 = da1c.getRawArray();
        ComplexFloatLargeArray resca = resc.getRawArray();

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(LargeArrayArithmetics.complexDiv(ca1.getComplexFloat(i * 3 + v), new float[]{da2.getFloatElement(i)[v], 0})[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), resca.getComplexFloat(i * 3 + v)[0], DELTA_F);
                assertEquals(FloatingPointUtils.processNaNs(LargeArrayArithmetics.complexDiv(ca1.getComplexFloat(i * 3 + v), new float[]{da2.getFloatElement(i)[v], 0})[1], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), resca.getComplexFloat(i * 3 + v)[1], DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        da1 = DataArray.create(td1, 1, "da1");

        ArrayList<Float> timeSeries2 = new ArrayList<>(2);
        timeSeries2.add(1.0f);
        timeSeries2.add(2.0f);
        ArrayList<LargeArray> dataSeries2 = new ArrayList<>(2);
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        TimeData td2 = new TimeData(timeSeries2, dataSeries2, timeSeries2.get(0));
        da2 = DataArray.create(td2, 2, "da2");

        res = DataArrayArithmetics.divF(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        td1 = td1.convertToFloat();
        td2 = td2.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td1.getValue(time);
            LargeArray b = td2.getValue(time);
            LargeArray c = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(a.getFloat(i) / b.getFloat(i * 2 + v), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testDivD()
    {
        int n = 10;

        //constant arrays
        DataArray da1 = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da1");
        DataArray da2 = DataArray.createConstant(DataArrayType.FIELD_DATA_INT, n, 100, "da2");
        DataArray res = DataArrayArithmetics.divD(da1, da2);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(da1.getDoubleElement(i)[0] / da2.getDoubleElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 = veclen2 = 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.UNSIGNED_BYTE, n), 1, "da1");
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.INT, n), 1, "da2");
        res = DataArrayArithmetics.divD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(da1.getDoubleElement(i)[0] / da2.getDoubleElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 = 1 && veclen2 > 1
        da2 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.INT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.divD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(da1.getDoubleElement(i)[0] / da2.getDoubleElement(i)[v], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //veclen1 > 1 && veclen2 == 1
        res = DataArrayArithmetics.divD(da2, da1);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(da2.getDoubleElement(i)[v] / da1.getDoubleElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //veclen1 = veclen2 > 1
        da1 = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.COMPLEX_FLOAT, 3 * n), 3, "da2");
        res = DataArrayArithmetics.divD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_COMPLEX, res.getType());
        assertEquals(3, res.getVectorLength());

        ComplexDataArray da1c = (ComplexDataArray) da1;
        ComplexDataArray resc = (ComplexDataArray) res;

        ComplexFloatLargeArray ca1 = da1c.getRawArray();
        ComplexFloatLargeArray resca = resc.getRawArray();

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(LargeArrayArithmetics.complexDiv(ca1.getComplexFloat(i * 3 + v), new float[]{da2.getFloatElement(i)[v], 0})[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), resca.getComplexFloat(i * 3 + v)[0], DELTA_F);
                assertEquals(FloatingPointUtils.processNaNs(LargeArrayArithmetics.complexDiv(ca1.getComplexFloat(i * 3 + v), new float[]{da2.getFloatElement(i)[v], 0})[1], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), resca.getComplexFloat(i * 3 + v)[1], DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries1 = new ArrayList<>(2);
        timeSeries1.add(0.0f);
        timeSeries1.add(1.0f);
        ArrayList<LargeArray> dataSeries1 = new ArrayList<>(2);
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        dataSeries1.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n));
        TimeData td1 = new TimeData(timeSeries1, dataSeries1, timeSeries1.get(0));
        da1 = DataArray.create(td1, 1, "da1");

        ArrayList<Float> timeSeries2 = new ArrayList<>(2);
        timeSeries2.add(1.0f);
        timeSeries2.add(2.0f);
        ArrayList<LargeArray> dataSeries2 = new ArrayList<>(2);
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        dataSeries2.add(LargeArrayUtils.generateRandom(LargeArrayType.INT, 2 * n));
        TimeData td2 = new TimeData(timeSeries2, dataSeries2, timeSeries2.get(0));
        da2 = DataArray.create(td2, 2, "da2");

        res = DataArrayArithmetics.divD(da1, da2);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        ArrayList<Float> timeSeries = res.getTimeSeries();
        TimeData td = res.getTimeData();
        td1 = td1.convertToDouble();
        td2 = td2.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td1.getValue(time);
            LargeArray b = td2.getValue(time);
            LargeArray c = td.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(a.getDouble(i) / b.getDouble(i * 2 + v), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }
    }

    @Test
    public void testPowF()
    {
        int n = 10;
        double e = 2.5;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.powF(da, e);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(pow(da.getFloatElement(i)[0], e), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.powF(da, e);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(pow(da.getFloatElement(i)[0], e), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        res = DataArrayArithmetics.powF(da, da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(pow(da.getFloatElement(i)[0], da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.powF(da, e);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(pow(da.getFloatElement(i)[v], e), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v], DELTA_F);
            }
        }

        res = DataArrayArithmetics.powF(da, da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(pow(da.getFloatElement(i)[v], da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.powF(da, e);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(pow(a.getFloat(i * 2 + v), e), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }

        res = DataArrayArithmetics.powF(da, da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(pow(a.getFloat(i * 2 + v), a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testPowD()
    {
        int n = 10;
        double e = 2.5;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.powD(da, e);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(pow(da.getDoubleElement(i)[0], e), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.powD(da, e);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(pow(da.getDoubleElement(i)[0], e), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        res = DataArrayArithmetics.powD(da, da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(pow(da.getDoubleElement(i)[0], da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.powD(da, e);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(pow(da.getDoubleElement(i)[v], e), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        res = DataArrayArithmetics.powD(da, da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(pow(da.getDoubleElement(i)[v], da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.powD(da, e);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(pow(a.getDouble(i * 2 + v), e), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }

        res = DataArrayArithmetics.powD(da, da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(pow(a.getDouble(i * 2 + v), a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }
    }

    @Test
    public void testNegF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.negF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(-da.getFloatElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.negF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(-da.getFloatElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.negF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(-da.getFloatElement(i)[v], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.negF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(-a.getFloat(i * 2 + v), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testNegD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.negD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(-da.getDoubleElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.negD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(-da.getDoubleElement(i)[0], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.negD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(-da.getDoubleElement(i)[v], FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.negD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(-a.getDouble(i * 2 + v), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }
    }

    @Test
    public void testSqrtF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.sqrtF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(sqrt(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.sqrtF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(sqrt(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.sqrtF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(sqrt(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.sqrtF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(sqrt(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testSqrtD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.sqrtD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(sqrt(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.sqrtD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(sqrt(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.sqrtD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(sqrt(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.sqrtD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(sqrt(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }
    }

    @Test
    public void testLogF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.logF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(log(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.logF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(log(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.logF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(log(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.logF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(log(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testLogD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.logD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(log(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.logD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(log(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.logD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(log(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.logD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(log(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }
    }

    @Test
    public void testLog10F()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.log10F(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(log10(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.log10F(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(log10(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.log10F(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(log10(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.log10F(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(log10(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testLog10D()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.log10D(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(log10(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.log10D(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(log10(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.log10D(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(log10(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.log10D(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(log10(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }
    }

    @Test
    public void testExpF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.expF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(exp(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.expF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(exp(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.expF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(exp(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.expF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(exp(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testExpD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.expD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(exp(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.expD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(exp(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.expD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(exp(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.expD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(exp(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }
    }

    @Test
    public void testAbsF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.absF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(abs(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.absF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(abs(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.absF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(1, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            double norm = 0;
            for (int v = 0; v < 3; v++) {
                norm += da.getFloatElement(i)[v] * da.getFloatElement(i)[v];
            }
            assertEquals(FloatingPointUtils.processNaNs(sqrt(norm), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);

        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.absF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(1, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                double norm = 0;
                for (int v = 0; v < 2; v++) {
                    norm += a.getFloat(i * 2 + v) * a.getFloat(i * 2 + v);
                }
                assertEquals(FloatingPointUtils.processNaNs(sqrt(norm), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i), DELTA_F);
            }
        }
    }

    @Test
    public void testAbsD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.absD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(abs(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.absD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(abs(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.absD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(1, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            double norm = 0;
            for (int v = 0; v < 3; v++) {
                norm += da.getDoubleElement(i)[v] * da.getDoubleElement(i)[v];
            }
            assertEquals(FloatingPointUtils.processNaNs(sqrt(norm), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_F);

        }
        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.absD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(1, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                double norm = 0;
                for (int v = 0; v < 2; v++) {
                    norm += a.getDouble(i * 2 + v) * a.getDouble(i * 2 + v);
                }
                assertEquals(FloatingPointUtils.processNaNs(sqrt(norm), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i), DELTA_F);

            }
        }
    }

    @Test
    public void testSinF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.sinF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(sin(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.sinF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(sin(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.sinF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(sin(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.sinF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(sin(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testSinD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.sinD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(sin(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.sinD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(sin(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.sinD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(sin(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.sinD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(sin(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }
    }

    @Test
    public void testCosF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.cosF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(cos(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.cosF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(cos(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.cosF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(cos(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.cosF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(cos(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testCosD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.cosD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(cos(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.cosD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(cos(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.cosD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(cos(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.cosD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(cos(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }
    }

    @Test
    public void testTanF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.tanF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(tan(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.tanF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(tan(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.tanF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(tan(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.tanF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(tan(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testTanD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.tanD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(tan(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.tanD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(tan(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.tanD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(tan(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.tanD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(tan(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }
    }

    @Test
    public void testAsinF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.asinF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(asin(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.asinF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(asin(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.asinF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(asin(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.asinF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(asin(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testAsinD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.asinD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(asin(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.asinD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(asin(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.asinD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(asin(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.asinD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(asin(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }
    }

    @Test
    public void testAcosF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.acosF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(acos(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.acosF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(acos(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.acosF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(acos(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.acosF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(acos(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testAcosD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.acosD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(acos(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.acosD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(acos(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.acosD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(acos(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.acosD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(acos(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }
    }

    @Test
    public void testAtanF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.atanF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(atan(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.atanF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(atan(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.atanF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(atan(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.atanF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(atan(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testAtanD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.atanD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(atan(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.atanD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(atan(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.atanD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(atan(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.atanD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(atan(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }
    }

    @Test
    public void testSignumF()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.signumF(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(signum(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.signumF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(signum(da.getFloatElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[0], DELTA_F);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.signumF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(signum(da.getFloatElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getFloatElement(i)[v], DELTA_F);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.signumF(da);

        assertEquals(DataArrayType.FIELD_DATA_FLOAT, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToFloat();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(signum(a.getFloat(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getFloat(i * 2 + v), DELTA_F);
                }
            }
        }
    }

    @Test
    public void testSignumD()
    {
        int n = 10;
        //constant array
        DataArray da = DataArray.createConstant(DataArrayType.FIELD_DATA_BYTE, n, 1, "da");
        DataArray res = DataArrayArithmetics.signumD(da);
        assertEquals(true, res.isConstant());
        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(signum(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen = 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, n), 1, "da");
        res = DataArrayArithmetics.signumD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        for (int i = 0; i < n; i++) {
            assertEquals(FloatingPointUtils.processNaNs(signum(da.getDoubleElement(i)[0]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[0], DELTA_D);
        }

        //veclen1 > 1
        da = DataArray.create(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 3 * n), 3, "da");
        res = DataArrayArithmetics.signumD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(3, res.getVectorLength());

        for (int i = 0; i < n; i++) {
            for (int v = 0; v < 3; v++) {
                assertEquals(FloatingPointUtils.processNaNs(signum(da.getDoubleElement(i)[v]), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), res.getDoubleElement(i)[v], DELTA_D);
            }
        }

        //time steps > 1
        ArrayList<Float> timeSeries = new ArrayList<>(3);
        timeSeries.add(0.0f);
        timeSeries.add(1.0f);
        timeSeries.add(2.0f);
        ArrayList<LargeArray> dataSeries = new ArrayList<>(3);
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        dataSeries.add(LargeArrayUtils.generateRandom(LargeArrayType.DOUBLE, 2 * n));
        TimeData td = new TimeData(timeSeries, dataSeries, timeSeries.get(0));
        da = DataArray.create(td, 2, "da");

        res = DataArrayArithmetics.signumD(da);

        assertEquals(DataArrayType.FIELD_DATA_DOUBLE, res.getType());
        assertEquals(2, res.getVectorLength());
        assertEquals(3, res.getTimeSeries().size());

        timeSeries = res.getTimeSeries();
        TimeData tdres = res.getTimeData();
        td = td.convertToDouble();
        for (Float time : timeSeries) {
            LargeArray a = td.getValue(time);
            LargeArray c = tdres.getValue(time);
            for (int i = 0; i < n; i++) {
                for (int v = 0; v < 2; v++) {
                    assertEquals(FloatingPointUtils.processNaNs(signum(a.getDouble(i * 2 + v)), FloatingPointUtils.defaultNanAction, FloatingPointUtils.defaultInfinityAction), c.getDouble(i * 2 + v), DELTA_D);
                }
            }
        }
    }
}
