/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic;

import java.util.ArrayList;
import pl.edu.icm.jscic.dataarrays.DataArraySchema;

/**
 * Holds general information about RegularField.
 * 
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class RegularFieldSchema extends FieldSchema
{

    /**
     * Number of dimensions
     */
    private int nDims = 3;

    /**
     * Creates a new instance of RegularFieldSchema.
     */
    public RegularFieldSchema()
    {
        super();
    }

    /**
     * Creates a new instance of RegularFieldSchema.
     * 
     * @param name field name
     */
    public RegularFieldSchema(String name)
    {
        super(name);
    }

    /**
     * Returns the number of dimensions.
     * 
     * @return number of dimensions
     */
    public int getNDims()
    {
        return nDims;
    }

    /**
     * Sets the number of dimensions.
     * 
     * @param nDims number of dimensions
     */
    public void setNDims(int nDims)
    {
        this.nDims = nDims;
    }

    /**
     * Compares two RegularFieldSchemas. Returns true if all components are compatible in this schema and the input schema and both fields are of the same
     * dimension, false otherwise.
     * 
     * @param	s RegularFieldSchema to be compared
     * 
     * @return	true if all components are compatible in this schema and the input schema and both fields are of the same dimension, false otherwise
     */
    public boolean isCompatibleWith(RegularFieldSchema s)
    {
        return super.isCompatibleWith(s) && nDims == s.getNDims();
    }

    @Override
    public RegularFieldSchema cloneDeep()
    {
        RegularFieldSchema clone = new RegularFieldSchema(this.name);

        ArrayList<DataArraySchema> componentSchemasClone = new ArrayList<>();
        if (this.componentSchemas != null && this.componentSchemas.size() > 0) {
            for (DataArraySchema item : this.componentSchemas) componentSchemasClone.add(item.cloneDeep());
        }
        clone.componentSchemas = componentSchemasClone;

        ArrayList<DataArraySchema> pseudoComponentSchemasClone = new ArrayList<>();
        if (this.pseudoComponentSchemas != null && this.pseudoComponentSchemas.size() > 0) {
            for (DataArraySchema item : this.pseudoComponentSchemas) pseudoComponentSchemasClone.add(item.cloneDeep());
        }
        clone.pseudoComponentSchemas = pseudoComponentSchemasClone;
        clone.nDims = nDims;
        return clone;
    }

}
