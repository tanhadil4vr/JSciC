/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic.utils;

import static org.apache.commons.math3.util.FastMath.*;

/**
 * Various operations on vectors.
 * 
 * @author Bartosz Borucki (babor@icm.edu.pl), University of Warsaw, ICM
 * @author Piotr Wendykier (piotrw@icm.edu.pl), University of Warsaw, ICM
 *
 */
public class VectorMath
{

    /**
     * Computes the sum of vector elements.
     * 
     * @param v vector
     * 
     * @return sum of vector elements, 0 for empty vector
     */
    public static int vectorSum(int[] v)
    {
        int sum = 0;
        for (int i = 0; i < v.length; i++) sum += v[i];
        return sum;
    }

    /**
     * Computes the sum of vector elements.
     * 
     * @param v vector
     * 
     * @return sum of vector elements, 0 for empty vector
     */
    public static float vectorSum(float[] v)
    {
        float sum = 0;
        for (int i = 0; i < v.length; i++) sum += v[i];
        return sum;
    }

    /**
     * Computes the sum of vector elements.
     * 
     * @param v vector
     * 
     * @return sum of vector elements, 0 for empty vector
     */
    public static double vectorSum(double[] v)
    {
        double sum = 0;
        for (int i = 0; i < v.length; i++) sum += v[i];
        return sum;
    }

    /**
     * Computes the cumulative sum of vector elements.
     * 
     * @param v       vector
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return cumulative sum of vector elements
     */
    public static float[] vectorCumulativeSum(float[] v, boolean inPlace)
    {
        float[] cumSum = new float[v.length];
        if (v.length > 0) {
            cumSum[0] = v[0];
            for (int i = 1; i < v.length; i++) cumSum[i] = cumSum[i - 1] + v[i];
        }
        if (inPlace) {
            System.arraycopy(cumSum, 0, v, 0, v.length);
            return v;
        }
        return cumSum;
    }

    /**
     * Computes the cumulative sum of vector elements.
     * 
     * @param v       vector
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return cumulative sum of vector elements
     */
    public static double[] vectorCumulativeSum(double[] v, boolean inPlace)
    {
        double[] cumSum = new double[v.length];
        if (v.length > 0) {
            cumSum[0] = v[0];
            for (int i = 1; i < v.length; i++) cumSum[i] = cumSum[i - 1] + v[i];
        }
        if (inPlace) {
            System.arraycopy(cumSum, 0, v, 0, v.length);
            return v;
        }
        return cumSum;
    }

    /**
     * Computes the product of vector elements.
     * 
     * @param v vector
     * 
     * @return product of vector elements, 1 for empty vector
     */
    public static float vectorProduct(float[] v)
    {
        float product = 1;
        for (int i = 0; i < v.length; i++) product *= v[i];
        return product;
    }

    /**
     * Computes the product of vector elements.
     * 
     * @param v vector
     * 
     * @return product of vector elements, 1 for empty vector
     */
    public static double vectorProduct(double[] v)
    {
        double product = 1;
        for (int i = 0; i < v.length; i++) product *= v[i];
        return product;
    }

    /**
     * Computes the angle between two vectors.
     * 
     * @param v1        first vector
     * @param v2        second vector
     * @param inDegrees if true, then the result is returned in degrees, otherwise in radians
     * 
     * @return angle between two vectors
     */
    public static float vectorAngle(float[] v1, float[] v2, boolean inDegrees)
    {
        float v1norm = vectorNorm(v1);
        float v2norm = vectorNorm(v2);
        float dotv1v2 = dotProduct(v1, v2);

        float cos = dotv1v2 / (v1norm * v2norm);
        if (cos > 1) {
            cos = 1;
        }
        if (cos < -1) {
            cos = -1;
        }
        float angle = (float) acos(cos);
        if (inDegrees) {
            angle = (float) (180 * angle / PI);
        }
        return angle;
    }

    /**
     * Computes the angle between two vectors.
     * 
     * @param v1        first vector
     * @param v2        second vector
     * @param inDegrees if true, then the result is returned in degrees, otherwise in radians
     * 
     * @return angle between two vectors
     */
    public static double vectorAngle(double[] v1, double[] v2, boolean inDegrees)
    {
        double v1norm = vectorNorm(v1);
        double v2norm = vectorNorm(v2);
        double dotv1v2 = dotProduct(v1, v2);

        double cos = dotv1v2 / (v1norm * v2norm);
        if (cos > 1) {
            cos = 1;
        }
        if (cos < -1) {
            cos = -1;
        }
        double angle = acos(cos);
        if (inDegrees) {
            angle = 180 * angle / PI;
        }
        return angle;
    }

    /**
     * Computes the sum of two vectors.
     * 
     * @param v1      first vector
     * @param v2      second vector
     * @param inPlace if true, then the operation is performed in-place (v1 = v1+v2)
     * 
     * @return sum of two vectors
     */
    public static float[] vectorAdd(float[] v1, float[] v2, boolean inPlace)
    {
        if (v1.length != v2.length) {
            throw new IllegalArgumentException("v1.length != v2.length");
        }
        if (inPlace) {
            for (int i = 0; i < v1.length; i++) {
                v1[i] += v2[i];
            }
            return v1;
        } else {
            float[] out = new float[v1.length];
            for (int i = 0; i < v1.length; i++) {
                out[i] = v1[i] + v2[i];
            }
            return out;
        }
    }

    /**
     * Computes the sum of two vectors.
     * 
     * @param v1      first vector
     * @param v2      second vector
     * @param inPlace if true, then the operation is performed in-place (v1 = v1+v2)
     * 
     * @return sum of two vectors
     */
    public static double[] vectorAdd(double[] v1, double[] v2, boolean inPlace)
    {
        if (v1.length != v2.length) {
            throw new IllegalArgumentException("v1.length != v2.length");
        }
        if (inPlace) {
            for (int i = 0; i < v1.length; i++) {
                v1[i] += v2[i];
            }
            return v1;
        } else {
            double[] out = new double[v1.length];
            for (int i = 0; i < v1.length; i++) {
                out[i] = v1[i] + v2[i];
            }
            return out;
        }
    }

    /**
     * Adds a scalar to each element of a vector.
     * 
     * @param v       vector
     * @param a       scalar
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return sum of a vector and a scalar
     */
    public static float[] vectorAddScalar(float[] v, float a, boolean inPlace)
    {
        if (inPlace) {
            for (int i = 0; i < v.length; i++) {
                v[i] += a;
            }
            return v;
        } else {
            float[] out = new float[v.length];
            for (int i = 0; i < v.length; i++) {
                out[i] = v[i] + a;
            }
            return out;
        }
    }

    /**
     * Adds a scalar to each element of a vector.
     * 
     * @param v       vector
     * @param a       scalar
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return sum of a vector and a scalar
     */
    public static double[] vectorAddScalar(double[] v, double a, boolean inPlace)
    {
        if (inPlace) {
            for (int i = 0; i < v.length; i++) {
                v[i] += a;
            }
            return v;
        } else {
            double[] out = new double[v.length];
            for (int i = 0; i < v.length; i++) {
                out[i] = v[i] + a;
            }
            return out;
        }
    }

    /**
     * Computes the difference of two vectors (v1 - v2).
     * 
     * @param v1      first vector
     * @param v2      second vector
     * @param inPlace if true, then the operation is performed in-place (v1 = v1-v2)
     * 
     * @return difference of two vectors (v1 - v2)
     */
    public static float[] vectorSubtract(float[] v1, float[] v2, boolean inPlace)
    {
        if (v1.length != v2.length) {
            throw new IllegalArgumentException("v1.length != v2.length");
        }
        if (inPlace) {
            for (int i = 0; i < v1.length; i++) {
                v1[i] -= v2[i];
            }
            return v1;
        } else {
            float[] out = new float[v1.length];
            for (int i = 0; i < v1.length; i++) {
                out[i] = v1[i] - v2[i];
            }
            return out;
        }
    }

    /**
     * Computes the difference of two vectors (v1 - v2).
     * 
     * @param v1      first vector
     * @param v2      second vector
     * @param inPlace if true, then the operation is performed in-place (v1 = v1-v2)
     * 
     * @return difference of two vectors (v1 - v2)
     */
    public static double[] vectorSubtract(double[] v1, double[] v2, boolean inPlace)
    {
        if (v1.length != v2.length) {
            throw new IllegalArgumentException("v1.length != v2.length");
        }
        if (inPlace) {
            for (int i = 0; i < v1.length; i++) {
                v1[i] -= v2[i];
            }
            return v1;
        } else {
            double[] out = new double[v1.length];
            for (int i = 0; i < v1.length; i++) {
                out[i] = v1[i] - v2[i];
            }
            return out;
        }
    }

    /**
     * Subtracts a scalar from each element of a vector (v - a).
     * 
     * @param v       vector
     * @param a       scalar
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return difference of a vector and a scalar (v - a)
     */
    public static float[] vectorSubtractScalar(float[] v, float a, boolean inPlace)
    {
        if (inPlace) {
            for (int i = 0; i < v.length; i++) {
                v[i] -= a;
            }
            return v;
        } else {
            float[] out = new float[v.length];
            for (int i = 0; i < v.length; i++) {
                out[i] = v[i] - a;
            }
            return out;
        }
    }

    /**
     * Subtracts a scalar from each element of a vector (v - a).
     * 
     * @param v       vector
     * @param a       scalar
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return difference of a vector and a scalar (v - a)
     */
    public static double[] vectorSubtractScalar(double[] v, double a, boolean inPlace)
    {
        if (inPlace) {
            for (int i = 0; i < v.length; i++) {
                v[i] -= a;
            }
            return v;
        } else {
            double[] out = new double[v.length];
            for (int i = 0; i < v.length; i++) {
                out[i] = v[i] - a;
            }
            return out;
        }
    }

    /**
     * Computes the product of two vectors.
     * 
     * @param v1      first vector
     * @param v2      second vector
     * @param inPlace if true, then the operation is performed in-place (v1 = v1*v2)
     * 
     * @return product of two vectors
     */
    public static float[] vectorMultiply(float[] v1, float[] v2, boolean inPlace)
    {
        if (v1.length != v2.length) {
            throw new IllegalArgumentException("v1.length != v2.length");
        }
        if (inPlace) {
            for (int i = 0; i < v1.length; i++) {
                v1[i] *= v2[i];
            }
            return v1;
        } else {
            float[] out = new float[v1.length];
            for (int i = 0; i < v1.length; i++) {
                out[i] = v1[i] * v2[i];
            }
            return out;
        }
    }

    /**
     * Computes the product of two vectors.
     * 
     * @param v1      first vector
     * @param v2      second vector
     * @param inPlace if true, then the operation is performed in-place (v1 = v1*v2)
     * 
     * @return product of two vectors
     */
    public static double[] vectorMultiply(double[] v1, double[] v2, boolean inPlace)
    {
        if (v1.length != v2.length) {
            throw new IllegalArgumentException("v1.length != v2.length");
        }
        if (inPlace) {
            for (int i = 0; i < v1.length; i++) {
                v1[i] *= v2[i];
            }
            return v1;
        } else {
            double[] out = new double[v1.length];
            for (int i = 0; i < v1.length; i++) {
                out[i] = v1[i] * v2[i];
            }
            return out;
        }
    }

    /**
     * Multiplies a scalar by each element of a vector.
     * 
     * @param v       vector
     * @param a       scalar
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return product of a vector and a scalar
     */
    public static float[] vectorMultiplyScalar(float[] v, float a, boolean inPlace)
    {
        if (inPlace) {
            for (int i = 0; i < v.length; i++) {
                v[i] *= a;
            }
            return v;
        } else {
            float[] out = new float[v.length];
            for (int i = 0; i < v.length; i++) {
                out[i] = v[i] * a;
            }
            return out;
        }
    }

    /**
     * Multiplies a scalar by each element of a vector.
     * 
     * @param v       vector
     * @param a       scalar
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return product of a vector and a scalar
     */
    public static double[] vectorMultiplyScalar(double[] v, double a, boolean inPlace)
    {
        if (inPlace) {
            for (int i = 0; i < v.length; i++) {
                v[i] *= a;
            }
            return v;
        } else {
            double[] out = new double[v.length];
            for (int i = 0; i < v.length; i++) {
                out[i] = v[i] * a;
            }
            return out;
        }
    }

    /**
     * Computes the quotient of two vectors (v1 / v2).
     * 
     * @param v1      first vector
     * @param v2      second vector
     * @param inPlace if true, then the operation is performed in-place (v1 = v1 / v2)
     * 
     * @return quotient of two vectors (v1 / v2)
     */
    public static float[] vectorDivide(float[] v1, float[] v2, boolean inPlace)
    {
        if (v1.length != v2.length) {
            throw new IllegalArgumentException("v1.length != v2.length");
        }
        if (inPlace) {
            for (int i = 0; i < v1.length; i++) {
                if (v2[i] == 0) {
                    throw new ArithmeticException("Division by zero!");
                }
                v1[i] /= v2[i];
            }
            return v1;
        } else {
            float[] out = new float[v1.length];
            for (int i = 0; i < v1.length; i++) {
                if (v2[i] == 0) {
                    throw new ArithmeticException("Division by zero!");
                }
                out[i] = v1[i] / v2[i];
            }
            return out;
        }
    }

    /**
     * Computes the quotient of two vectors (v1 / v2).
     * 
     * @param v1      first vector
     * @param v2      second vector
     * @param inPlace if true, then the operation is performed in-place (v1 = v1 / v2)
     * 
     * @return quotient of two vectors (v1 / v2)
     */
    public static double[] vectorDivide(double[] v1, double[] v2, boolean inPlace)
    {
        if (v1.length != v2.length) {
            throw new IllegalArgumentException("v1.length != v2.length");
        }
        if (inPlace) {
            for (int i = 0; i < v1.length; i++) {
                if (v2[i] == 0) {
                    throw new ArithmeticException("Division by zero!");
                }
                v1[i] /= v2[i];
            }
            return v1;
        } else {
            double[] out = new double[v1.length];
            for (int i = 0; i < v1.length; i++) {
                if (v2[i] == 0) {
                    throw new ArithmeticException("Division by zero!");
                }
                out[i] = v1[i] / v2[i];
            }
            return out;
        }
    }

    /**
     * Divides each element of a vector by a scalar.
     * 
     * @param v       vector
     * @param a       scalar
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return quotient of a vector and a scalar
     */
    public static float[] vectorDivideScalar(float[] v, float a, boolean inPlace)
    {
        if (a == 0) {
            throw new ArithmeticException("Division by zero!");
        }
        if (inPlace) {
            for (int i = 0; i < v.length; i++) {
                v[i] /= a;
            }
            return v;
        } else {
            float[] out = new float[v.length];
            for (int i = 0; i < v.length; i++) {
                out[i] = v[i] / a;
            }
            return out;
        }
    }

    /**
     * Divides each element of a vector by a scalar.
     * 
     * @param v       vector
     * @param a       scalar
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return quotient of a vector and a scalar
     */
    public static double[] vectorDivideScalar(double[] v, double a, boolean inPlace)
    {
        if (a == 0) {
            throw new ArithmeticException("Division by zero!");
        }
        if (inPlace) {
            for (int i = 0; i < v.length; i++) {
                v[i] /= a;
            }
            return v;
        } else {
            double[] out = new double[v.length];
            for (int i = 0; i < v.length; i++) {
                out[i] = v[i] / a;
            }
            return out;
        }
    }

    /**
     * Computes the dot product of two vectors.
     * 
     * @param v1 first vector
     * @param v2 second vector
     * 
     * @return dot product
     */
    public static double dotProduct(double[] v1, double[] v2)
    {
        if (v1.length != v2.length) {
            throw new IllegalArgumentException("v1.length != v2.length");
        }
        switch (v1.length) {
            case 2:
                return dotProduct2(v1, v2);
            case 3:
                return dotProduct3(v1, v2);
            default:
                double out = 0;
                for (int i = 0; i < v1.length; i++) {
                    out += v1[i] * v2[i];
                }
                return out;
        }
    }

    /**
     * Computes the dot product of two vectors.
     * 
     * @param v1 first vector
     * @param v2 second vector
     * 
     * @return dot product
     */
    public static float dotProduct(float[] v1, float[] v2)
    {
        if (v1.length != v2.length) {
            throw new IllegalArgumentException("v1.length != v2.length");
        }
        switch (v1.length) {
            case 2:
                return dotProduct2(v1, v2);
            case 3:
                return dotProduct3(v1, v2);
            default:
                float out = 0;
                for (int i = 0; i < v1.length; i++) {
                    out += v1[i] * v2[i];
                }
                return out;
        }
    }

    /**
     * Computes the dot product of two vectors of length 2.
     * 
     * @param v1 first vector
     * @param v2 second vector
     * 
     * @return dot product
     */
    public static double dotProduct2(double[] v1, double[] v2)
    {
        return v1[0] * v2[0] + v1[1] * v2[1];
    }

    /**
     * Computes the dot product of two vectors of length 2.
     * 
     * @param v1 first vector
     * @param v2 second vector
     * 
     * @return dot product
     */
    public static float dotProduct2(float[] v1, float[] v2)
    {
        return v1[0] * v2[0] + v1[1] * v2[1];
    }

    /**
     * Computes the dot product of two vectors of length 3.
     * 
     * @param v1 first vector
     * @param v2 second vector
     * 
     * @return dot product
     */
    public static double dotProduct3(double[] v1, double[] v2)
    {
        return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
    }

    /**
     * Computes the dot product of two vectors of length 3.
     * 
     * @param v1 first vector
     * @param v2 second vector
     * 
     * @return dot product
     */
    public static float dotProduct3(float[] v1, float[] v2)
    {
        return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
    }

    /**
     * Computes the cross product of two vectors of length 3.
     * 
     * @param v1      first vector
     * @param v2      second vector
     * @param inPlace if true, then the operation is performed in-place (v1 = v1 x v2)
     * 
     * @return cross product
     */
    public static double[] crossProduct(double[] v1, double[] v2, boolean inPlace)
    {
        double[] out = new double[3];
        out[0] = v1[1] * v2[2] - v1[2] * v2[1];
        out[1] = v1[2] * v2[0] - v1[0] * v2[2];
        out[2] = v1[0] * v2[1] - v1[1] * v2[0];
        if (inPlace) {
            System.arraycopy(out, 0, v1, 0, 3);
            return v1;
        }
        return out;
    }

    /**
     * Computes the cross product of two vectors of length 3.
     * 
     * @param v1      first vector
     * @param v2      second vector
     * @param inPlace if true, then the operation is performed in-place (v1 = v1 x v2)
     * 
     * @return cross product
     */
    public static float[] crossProduct(float[] v1, float[] v2, boolean inPlace)
    {
        float[] out = new float[3];
        out[0] = v1[1] * v2[2] - v1[2] * v2[1];
        out[1] = v1[2] * v2[0] - v1[0] * v2[2];
        out[2] = v1[0] * v2[1] - v1[1] * v2[0];
        if (inPlace) {
            System.arraycopy(out, 0, v1, 0, 3);
            return v1;
        }
        return out;
    }

    /**
     * Returns the minimum value of a vector.
     * 
     * @param v vector
     * 
     * @return minimum value
     */
    public static int vectorMin(int[] v)
    {
        if (v.length == 0) throw new IllegalArgumentException("Empty arrays are not allowed here");
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < v.length; i++) {
            int value = v[i];
            if (min > value) min = value;
        }
        return min;
    }

    /**
     * Returns the minimum value of a vector.
     * 
     * @param v vector
     * 
     * @return minimum value
     */
    public static float vectorMin(float[] v)
    {
        if (v.length == 0) throw new IllegalArgumentException("Empty arrays are not allowed here");
        float min = Float.MAX_VALUE;
        for (int i = 0; i < v.length; i++) {
            float value = v[i];
            if (min > value) min = value;
        }
        return min;
    }

    /**
     * Returns the minimum value of a vector.
     * 
     * @param v vector
     * 
     * @return minimum value
     */
    public static double vectorMin(double[] v)
    {
        if (v.length == 0) throw new IllegalArgumentException("Empty arrays are not allowed here");
        double min = Double.MAX_VALUE;
        for (int i = 0; i < v.length; i++) {
            double value = v[i];
            if (min > value) min = value;
        }
        return min;
    }

    /**
     * Returns the maximum value of a vector.
     * 
     * @param v vector
     * 
     * @return maximum value
     */
    public static int vectorMax(int[] v)
    {
        if (v.length == 0) throw new IllegalArgumentException("Empty arrays are not allowed here");
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < v.length; i++) {
            int value = v[i];
            if (max < value) max = value;
        }
        return max;
    }

    /**
     * Returns the maximum value of a vector.
     * 
     * @param v vector
     * 
     * @return maximum value
     */
    public static float vectorMax(float[] v)
    {
        if (v.length == 0) throw new IllegalArgumentException("Empty arrays are not allowed here");
        float max = -Float.MAX_VALUE;
        for (int i = 0; i < v.length; i++) {
            float value = v[i];
            if (max < value) max = value;
        }
        return max;
    }

    /**
     * Returns the maximum value of a vector.
     * 
     * @param v vector
     * 
     * @return maximum value
     */
    public static double vectorMax(double[] v)
    {
        if (v.length == 0) throw new IllegalArgumentException("Empty arrays are not allowed here");
        double max = -Double.MAX_VALUE;
        for (int i = 0; i < v.length; i++) {
            double value = v[i];
            if (max < value) max = value;
        }
        return max;
    }

    /**
     * Reverses the elements in a vector.
     * 
     * @param v       vector
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return vector with reversed elements
     */
    public static int[] vectorReverse(int[] v, boolean inPlace)
    {
        if (inPlace) {
            int end = v.length - 1;
            for (int i = 0; i < v.length / 2; i++) {
                int t = v[i];
                v[i] = v[end];
                v[end] = t;
                end--;
            }
            return v;
        } else {
            int[] reversed = new int[v.length];
            int pos = v.length - 1;
            for (int i = 0; i < v.length; i++)
                reversed[i] = v[pos--];
            return reversed;
        }
    }

    /**
     * Reverses the elements in a vector.
     * 
     * @param v       vector
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return vector with reversed elements
     */
    public static float[] vectorReverse(float[] v, boolean inPlace)
    {
        if (inPlace) {
            int end = v.length - 1;
            for (int i = 0; i < v.length / 2; i++) {
                float t = v[i];
                v[i] = v[end];
                v[end] = t;
                end--;
            }
            return v;
        } else {
            float[] reversed = new float[v.length];
            int pos = v.length - 1;
            for (int i = 0; i < v.length; i++)
                reversed[i] = v[pos--];
            return reversed;
        }
    }

    /**
     * Reverses the elements in a vector.
     * 
     * @param v       vector
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return vector with reversed elements
     */
    public static double[] vectorReverse(double[] v, boolean inPlace)
    {
        if (inPlace) {
            int end = v.length - 1;
            for (int i = 0; i < v.length / 2; i++) {
                double t = v[i];
                v[i] = v[end];
                v[end] = t;
                end--;
            }
            return v;
        } else {
            double[] reversed = new double[v.length];
            int pos = v.length - 1;
            for (int i = 0; i < v.length; i++)
                reversed[i] = v[pos--];
            return reversed;
        }
    }

    /**
     * Computes the norm of a vector.
     * 
     * @param v vector
     * 
     * @return vector norm
     */
    public static double vectorNorm(double[] v)
    {
        double out = 0;
        for (int i = 0; i < v.length; i++) {
            out += v[i] * v[i];
        }
        return sqrt(out);
    }

    /**
     * Computes the norm of a vector.
     * 
     * @param v vector
     * 
     * @return vector norm
     */
    public static float vectorNorm(float[] v)
    {
        float out = 0;
        for (int i = 0; i < v.length; i++) {
            out += v[i] * v[i];
        }
        return (float) sqrt(out);
    }

    /**
     * Normalizes vector (each element is divided by the vector norm).
     * 
     * @param v       vector
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return normalized vector
     */
    public static float[] vectorNormalize(float[] v, boolean inPlace)
    {
        float norm = vectorNorm(v);
        if (norm > 0) {
            return vectorDivideScalar(v, norm, inPlace);
        } else {
            return v;
        }
    }

    /**
     * Normalizes vector (each element is divided by the vector norm).
     * 
     * @param v       vector
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return normalized vector
     */
    public static double[] vectorNormalize(double[] v, boolean inPlace)
    {
        double norm = vectorNorm(v);
        if (norm > 0) {
            return vectorDivideScalar(v, norm, inPlace);
        } else {
            return v;
        }
    }

    /**
     * Normalizes vector (each element is divided by the sum of all vector elements).
     * 
     * @param v       vector
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return normalized vector
     */
    public static float[] vectorNormalizeToOne(float[] v, boolean inPlace)
    {
        float total = vectorSum(v);
        if (total != 0) {
            return vectorDivideScalar(v, total, inPlace);
        } else {
            return v;
        }
    }

    /**
     * Normalizes vector (each element is divided by the sum of all vector elements).
     * 
     * @param v       vector
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return normalized vector
     */
    public static double[] vectorNormalizeToOne(double[] v, boolean inPlace)
    {
        double total = vectorSum(v);
        if (total != 0) {
            return vectorDivideScalar(v, total, inPlace);
        } else {
            return v;
        }
    }

    /**
     * Computes a vector perpendicular to a given vector.
     * 
     * @param v       vector
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return perpendicular vector
     */
    public static float[] vectorPerpendicular(float[] v, boolean inPlace)
    {
        float vmax = 0;
        int k = -1;
        for (int i = 0; i < v.length; i++) {
            if (abs(v[i]) > vmax) {
                vmax = v[i];
                k = i;
            }
        }
        int l = (k + 1) % v.length;
        float[] el = new float[v.length];
        el[l] = 1;
        float[] w = vectorNormalize(v, false);
        el = vectorSubtract(el, vectorMultiplyScalar(w, dotProduct(el, w), false), false);
        vectorNormalize(el, true);
        if (inPlace) {
            System.arraycopy(el, 0, v, 0, v.length);
            return v;
        }
        return el;
    }

    /**
     * Computes a vector perpendicular to a given vector.
     * 
     * @param v       vector
     * @param inPlace if true, then the operation is performed in-place
     * 
     * @return perpendicular vector
     */
    public static double[] vectorPerpendicular(double[] v, boolean inPlace)
    {
        double vmax = 0;
        int k = -1;
        for (int i = 0; i < v.length; i++) {
            if (abs(v[i]) > vmax) {
                vmax = v[i];
                k = i;
            }
        }
        int l = (k + 1) % v.length;
        double[] el = new double[v.length];
        el[l] = 1;
        double[] w = vectorNormalize(v, false);
        el = vectorSubtract(el, vectorMultiplyScalar(w, dotProduct(el, w), false), false);
        vectorNormalize(el, true);
        if (inPlace) {
            System.arraycopy(el, 0, v, 0, v.length);
            return v;
        }
        return el;
    }

    /**
     * Creates a vector from the endpoint coordinates.
     * 
     * @param p0 start coordinate
     * @param p1 end coordinate
     * 
     * @return vector
     */
    public static float[] vectorFromEndpoints(float[] p0, float[] p1)
    {
        if (p0.length != p1.length) {
            throw new IllegalArgumentException("p0.length != p1.length");
        }
        float[] out = new float[p0.length];
        for (int i = 0; i < p0.length; i++) {
            out[i] = p1[i] - p0[i];
        }
        return out;
    }

    /**
     * Creates a vector from the endpoint coordinates.
     * 
     * @param p0 start coordinate
     * @param p1 end coordinate
     * 
     * @return vector
     */
    public static double[] vectorFromEndpoints(double[] p0, double[] p1)
    {
        if (p0.length != p1.length) {
            throw new IllegalArgumentException("p0.length != p1.length");
        }
        double[] out = new double[p0.length];
        for (int i = 0; i < p0.length; i++) {
            out[i] = p1[i] - p0[i];
        }
        return out;
    }
}
