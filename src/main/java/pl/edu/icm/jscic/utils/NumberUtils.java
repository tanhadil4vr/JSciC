/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import static org.apache.commons.math3.util.FastMath.*;

/**
 * Utilities to operate on Number objects like boxed primitives and BigDecimal numbers, also different kind of conversion methods.
 * 
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class NumberUtils
{

    public static BigDecimal TWO = new BigDecimal(2);
    public static BigDecimal FIVE = new BigDecimal(5);

    /**
     * Returns most significant exponent which is equivalent to floor(log10(.)) . For 0.01 returns -2.
     * 
     * @param number input number
     * 
     * @return most significant exponent
     */
    public static int mostSignificantExponent(BigDecimal number)
    {
        return -number.scale() + number.precision() - 1;
    }

    /**
     * Returns number scaled down by highest power of 10 that is smaller then or equal to number.
     * Example: 123.4 is scaled to 1.234
     * 
     * @param number input number
     * 
     * @return number scaled down by highest power of 10 that is smaller then or equal to number
     */
    public static BigDecimal scaleToSingleDigit(BigDecimal number)
    {
        return number.scaleByPowerOfTen(-mostSignificantExponent(number));
    }

    /**
     * Returns highest power of 10 smaller then or equal to number.
     * 
     * @param number input number
     * 
     * @return highest power of 10 smaller then or equal to number
     */
    public static BigDecimal bottomPow10(BigDecimal number)
    {
        if (number.compareTo(BigDecimal.ZERO) <= 0) throw new IllegalArgumentException(number + " needs to be positive");
        else return new BigDecimal(BigInteger.ONE, -mostSignificantExponent(number));
    }

    /**
     * Returns largest exponent exp such that 10^exp &lt; number. Number must be positive, IllegalArgumentException is thrown otherwise.
     * 
     * @param number input number
     * 
     * @return largest exponent exp such that 10^exp &lt; number
     */
    public static int floorLog10(BigDecimal number)
    {
        if (number.compareTo(BigDecimal.ZERO) <= 0) throw new IllegalArgumentException(number + " needs to be positive");
        else return mostSignificantExponent(number);
    }

    /**
     * Returns smallest exponent exp such that 10^exp &gt; number. Number must be positive, IllegalArgumentException is thrown otherwise.
     * 
     * @param number input number
     * 
     * @return largest exponent exp such that 10^exp &lt; number
     */
    public static int ceilLog10(BigDecimal number)
    {
        if (number.compareTo(BigDecimal.ZERO) <= 0) throw new IllegalArgumentException(number + " needs to be positive");
        else return 1 + mostSignificantExponent(number.subtract(number.ulp().movePointLeft(1)));
    }

    /**
     * Returns 10 raised to the power of exponent.
     * 
     * @param exponent exponent
     * 
     * @return 10 raised to the power of exponent
     */
    public static BigDecimal pow10(int exponent)
    {
        return new BigDecimal(BigInteger.ONE, -exponent);
    }

    /**
     * Returns floor of number.
     * 
     * @param number input number
     * 
     * @return floor of number
     */
    public static BigDecimal floor(BigDecimal number)
    {
        return number.setScale(0, RoundingMode.FLOOR);
    }

    /**
     * Returns ceil of number.
     * 
     * @param number input number
     * 
     * @return ceil of number
     */
    public static BigDecimal ceil(BigDecimal number)
    {
        return number.setScale(0, RoundingMode.CEILING);
    }

    
    /**
     * Finds out if n1 and n2 are objects of the same class
     * 
     * @param n1 object to compare
     * @param n2 object to compare
     * 
     * @return true if object classes match
     */
    public static boolean isSameClass(Number n1, Number n2)
    {
        return n1.getClass().equals(n2.getClass());
    }

    /**
     * Returns true if number is a real number like Float, Double or BigDecimal
     * 
     * @param number input number
     * 
     * @return true if number is a real number
     */
    public static boolean isReal(Number number)
    {
        return number instanceof Float ||
            number instanceof Double ||
            number instanceof BigDecimal;
    }

    /**
     * Unwraps array of wrapped primitives
     * 
     * @param numbers array of wrapped primitives
     * 
     * @return numbers converted to array of primitives
     */
    public static double[] unbox(Double[] numbers)
    {
        //all these unbox methods could be done by one unbox(Number[] numbers) but there would be a problem with empty array
        //as we couldn't guess number type in such case.
        double[] numbersUnboxed = new double[numbers.length];
        for (int i = 0; i < numbers.length; i++) numbersUnboxed[i] = numbers[i];
        return numbersUnboxed;
    }

    /**
     * Unwraps array of wrapped primitives
     * 
     * @param numbers array of wrapped primitives
     * 
     * @return numbers converted to array of primitives
     */
    public static float[] unbox(Float[] numbers)
    {
        float[] numbersUnboxed = new float[numbers.length];
        for (int i = 0; i < numbers.length; i++) numbersUnboxed[i] = numbers[i];
        return numbersUnboxed;
    }

    /**
     * Unwraps array of wrapped primitives
     * 
     * @param numbers array of wrapped primitives
     * 
     * @return numbers converted to array of primitives
     */
    public static long[] unbox(Long[] numbers)
    {
        long[] numbersUnboxed = new long[numbers.length];
        for (int i = 0; i < numbers.length; i++) numbersUnboxed[i] = numbers[i];
        return numbersUnboxed;
    }

    /**
     * Unwraps array of wrapped primitives
     * 
     * @param numbers array of wrapped primitives
     * 
     * @return numbers converted to array of primitives
     */
    public static int[] unbox(Integer[] numbers)
    {
        int[] numbersUnboxed = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) numbersUnboxed[i] = numbers[i];
        return numbersUnboxed;
    }

    /**
     * Wraps array of primitives
     * 
     * @param numbers array of primitives
     * 
     * @return numbers wrapped into corresponding wrapping classes
     */
    public static Double[] box(double[] numbers)
    {
        Double[] numbersBoxed = new Double[numbers.length];
        for (int i = 0; i < numbers.length; i++) numbersBoxed[i] = numbers[i];
        return numbersBoxed;
    }

    /**
     * Wraps array of primitives
     * 
     * @param numbers array of primitives
     * 
     * @return numbers wrapped into corresponding wrapping classes
     */
    public static Float[] box(float[] numbers)
    {
        Float[] numbersBoxed = new Float[numbers.length];
        for (int i = 0; i < numbers.length; i++) numbersBoxed[i] = numbers[i];
        return numbersBoxed;
    }

    /**
     * Wraps array of primitives
     * 
     * @param numbers array of primitives
     * 
     * @return numbers wrapped into corresponding wrapping classes
     */
    public static Long[] box(long[] numbers)
    {
        Long[] numbersBoxed = new Long[numbers.length];
        for (int i = 0; i < numbers.length; i++) numbersBoxed[i] = numbers[i];
        return numbersBoxed;
    }

    /**
     * Rounds/truncates long value to int.
     * If number &gt; Integer.MAX_VALUE than result is Integer.MAX_VALUE. Similar for negative numbers.
     * 
     * @param number input number
     * 
     * @return int value
     */
    public static int truncateToInt(long number)
    {
        if (number > Integer.MAX_VALUE) return Integer.MAX_VALUE;
        else if (number < Integer.MIN_VALUE) return Integer.MIN_VALUE;
        else return (int) number;
    }

    /**
     * Converts array of numbers into Double with respect to their decimal representation (so 0.8f is converted to double 0.8).
     * Accuracy may be lower when converting from Long.
     * 
     * @param numbers input numbers
     * 
     * @return numbers converted to Double
     */
    public static Double[] convertToDouble(Number[] numbers)
    {
        Double[] numbersD = new Double[numbers.length];
        for (int i = 0; i < numbers.length; i++)
            numbersD[i] = convertToDouble(numbers[i]);//numbers[i].doubleValue();
        return numbersD;
    }

    /**
     * Converts array of numbers into Float using floatValue.
     * Accuracy may be lower when converting from Long.
     * 
     * @param numbers input numbers
     * 
     * @return numbers converted to Float
     */
    public static Float[] convertToFloat(Number[] numbers)
    {
        Float[] numbersF = new Float[numbers.length];
        for (int i = 0; i < numbers.length; i++)
            numbersF[i] = numbers[i].floatValue();
        return numbersF;
    }

    /**
     * Converts array of numbers into Long using longValue.
     * Double and Float are trimmed here.
     * 
     * @param numbers input number
     * 
     * @return numbers converted to Long
     */
    public static Long[] convertToLong(Number[] numbers)
    {
        Long[] numbersL = new Long[numbers.length];
        for (int i = 0; i < numbers.length; i++)
            numbersL[i] = numbers[i].longValue();
        return numbersL;
    }

    /**
     * Casts array of numbers into long.  Large doubles are trimmed.
     * 
     * @param numbers input numbers
     * 
     * @return numbers casted to long
     */
    public static long[] convertToLong(double[] numbers)
    {
        long[] numbersL = new long[numbers.length];
        for (int i = 0; i < numbers.length; i++) numbersL[i] = (long) numbers[i];
        return numbersL;
    }

    /**
     * Converts number to double with respect to its decimal representation (based on BigDecimal conversion).
     * Note: this conversion is different than binary conversion:
     * - binary (double)Float.MAX_VALUE gives 3.4028234663852886E38
     * - this conversion gives 3.4028235E38
     * so instead of convertToDouble(Float.MAX_VALUE) &gt; Float.MAX_VALUE (which gives true !! - there is hidden binary conversion to double)
     * one should use (float)convertToDouble(Float.MAX_VALUE) &gt; Float.MAX_VALUE.
     * 
     * @param number input number
     * 
     * @return converted number to double with respect to its decimal representation 
     */
    public static double convertToDouble(Number number)
    {
        //this is to avoid precision problems when converting from float (0.8) to double (0.800000011920929)
        return toBigDecimal(number).doubleValue();
    }

    /**
     * Converts Number[] to BigDecimal with respect to their decimal representation (based on BigDecimal(String))
     * 
     * @param numbers input numbers
     * 
     * @return numbers converted into BigDecimal with respect to their decimal representation 
     */
    public static BigDecimal[] toBigDecimal(Number[] numbers)
    {
        BigDecimal[] numbersBD = new BigDecimal[numbers.length];
        for (int i = 0; i < numbers.length; i++)
            numbersBD[i] = toBigDecimal(numbers[i]);
        return numbersBD;
    }

    /**
     * Converts Number to BigDecimal with respect to its decimal representation (based on BigDecimal(String))
     * 
     * @param number input number
     * 
     * @return number converted into BigDecimal with respect to its decimal representation 
     */
    public static BigDecimal toBigDecimal(Number number)
    {
        return new BigDecimal(number.toString());
    }

    /**
     * Converts float to BigDecimal with respect to its decimal representation (based on BigDecimal(String))
     * 
     * @param f input number
     * 
     * @return number converted into BigDecimal with respect to its decimal representation 
     */
    public static BigDecimal toBigDecimal(float f)
    {
        return new BigDecimal(Float.toString(f));
    }

    /**
     * Converts double to BigDecimal with respect to its decimal representation (based on BigDecimal(String))
     * 
     * @param d input number
     * 
     * @return number converted into BigDecimal with respect to its decimal representation 
     */
    public static BigDecimal toBigDecimal(double d)
    {
        return new BigDecimal(Double.toString(d));
    }

    /**
     * Converts int to BigDecimal
     * 
     * @param i input number
     * 
     * @return number converted into BigDecimal
     */
    public static BigDecimal toBigDecimal(int i)
    {
        return new BigDecimal(i);
    }

    /**
     * Converts long to BigDecimal
     * 
     * @param l input number
     * 
     * @return number converted into BigDecimal
     */
    public static BigDecimal toBigDecimal(long l)
    {
        return new BigDecimal(l);
    }

    /**
     * Converts floats to BigDecimals with respect to their decimal representation (based on BigDecimal(String))
     * 
     * @param fs input number
     * 
     * @return numbers converted into BigDecimal with respect to their decimal representation 
     */
    public static BigDecimal[] toBigDecimal(float[] fs)
    {
        BigDecimal[] bds = new BigDecimal[fs.length];
        for (int i = 0; i < bds.length; i++)
            bds[i] = new BigDecimal(Float.toString(fs[i]));
        return bds;
    }

    /**
     * Converts doubles to BigDecimals with respect to their decimal representation (based on BigDecimal(String))
     * 
     * @param ds input number
     * 
     * @return numbers converted into BigDecimal with respect to their decimal representation 
     */
    public static BigDecimal[] toBigDecimal(double[] ds)
    {
        BigDecimal[] bds = new BigDecimal[ds.length];
        for (int i = 0; i < bds.length; i++)
            bds[i] = new BigDecimal(Double.toString(ds[i]));
        return bds;
    }

    /**
     * Returns true if number is NaN or Infinity.
     * 
     * @param number input number
     * 
     * @return true if number is NaN or Infinity, false otherwise
     */
    public static boolean isNaNorInfinite(Number number)
    {
        return number instanceof Float && (((Float) number).isNaN() || ((Float) number).isInfinite()) ||
            number instanceof Double && (((Double) number).isNaN() || ((Double) number).isInfinite());
    }

    /**
     * Replaces Double.POSITIVE_INFINITY by Double.MAX_VALUE and Double.NEGATIVE_INFINITY by -Double.MAX_VALUE
     * 
     * @param number input number
     * 
     * @return number with infinity replaced by corresponding Max or -Max values
     */
    public static double removeInfinity(double number)
    {
        if (number == Double.POSITIVE_INFINITY) return Double.MAX_VALUE;
        else if (number == Double.NEGATIVE_INFINITY) return -Double.MAX_VALUE;
        else return number;
    }

    /**
     * Replaces Float.POSITIVE_INFINITY by Float.MAX_VALUE and float.NEGATIVE_INFINITY by -Float.MAX_VALUE
     * 
     * @param number input number
     * 
     * @return number with infinity replaced by corresponding Max or -Max values
     */
    public static float removeInfinity(float number)
    {
        if (number == Float.POSITIVE_INFINITY) return Float.MAX_VALUE;
        else if (number == Float.NEGATIVE_INFINITY) return -Float.MAX_VALUE;
        else return number;
    }

    /**
     * Clips the number to the specified range.
     * 
     * @param number input number
     * @param min    minimum value
     * @param max    maximum value
     * 
     * @return clipped value
     */
    public static float clip(float number, float min, float max)
    {
        return (float) max(min, min(max, number));
    }

    /**
     * Clips the number to the specified range.
     * 
     * @param number input number
     * @param min    minimum value
     * @param max    maximum value
     * 
     * @return clipped value
     */
    public static double clip(double number, double min, double max)
    {
        return max(min, min(max, number));
    }
}
