/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic;

import pl.edu.icm.jscic.dataarrays.DataArraySchema;
import pl.edu.icm.jscic.dataarrays.DataArray;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Holds general information about component schemas.
 * 
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class DataContainerSchema implements Serializable
{

    /**
     * Name of the container.
     */
    protected String name;

    /**
     * User specified data.
     */
    private String[] userData;

    /**
     * A list holding schemas of all data components in the field.
     */
    protected ArrayList<DataArraySchema> componentSchemas = new ArrayList<>();

    /**
     * A list holding schemas of pseudo-components (indices, coordinates, mask etc.) in the field.
     */
    protected ArrayList<DataArraySchema> pseudoComponentSchemas = new ArrayList<>();

    /**
     * Creates a new instance of DataContainerSchema.
     * 
     * @param name name of the schema
     */
    public DataContainerSchema(String name)
    {
        if (name.isEmpty()) throw new IllegalArgumentException("Data container name cannot be empty");
        this.name = name.replaceAll("\\W", "_");
    }

    /**
     * Returns true if the container is empty, false otherwise.
     * 
     * @return true if the container is empty, false otherwise
     */
    public boolean isEmpty()
    {
        return componentSchemas.isEmpty();
    }

    /**
     * Adds new schema at specified position.
     * 
     * @param pos position where new schema will be added. If pos is greater or equal current number of schemas,
     *            new schema will be added at the end.
     * @param	s	  new schema, this reference is used internally
     */
    public void addDataArraySchema(int pos, DataArraySchema s)
    {
        componentSchemas.add(pos, s);
    }

    /**
     * Adds new schema at the end of the list.
     * 
     * @param	s	new schema, this reference is used internally
     */
    public void addDataArraySchema(DataArraySchema s)
    {
        componentSchemas.add(s);
    }

    /**
     * Removes specified schema.
     * 
     * @param s schema
     */
    public void removeDataArraySchema(DataArraySchema s)
    {
        componentSchemas.remove(s);
    }

    /**
     * Add new pseudo-component schema at the end of the list.
     * 
     * @param	s	new schema, this reference is used internally
     */
    public void addPseudoComponentSchema(DataArraySchema s)
    {
        pseudoComponentSchemas.add(s);
    }

    /**
     * Removes specified schema.
     * 
     * @param s schema
     */
    public void removePseudoComponentSchema(DataArraySchema s)
    {
        pseudoComponentSchemas.remove(s);
    }

    /**
     * Adds schemas extracted from data components.
     * 
     * @param	data	list of components
     */
    public void setComponentSchemasFromData(ArrayList<DataArray> data)
    {
        componentSchemas.clear();
        for (int i = 0; i < data.size(); i++)
            componentSchemas.add(data.get(i).getSchema());
    }

    /**
     * Returns schema at specified position.
     * 
     * @param i position
     * 
     * @return schema
     */
    public DataArraySchema getSchema(int i)
    {
        return getComponentSchema(i);
    }

    /**
     * Returns a list of schemas. This method returns a reference to the internal list.
     * 
     * @return	list of schemas
     */
    public ArrayList<DataArraySchema> getComponentSchemas()
    {
        return componentSchemas;
    }

    /**
     * Returns the number of pseudo-components.
     * 
     * @return number of pseudo-components
     */
    public int getNPseudoComponents()
    {
        return pseudoComponentSchemas.size();
    }

    /**
     * Returns a pseudo-component schema at specified position.
     * 
     * @param i position
     * 
     * @return schema
     */
    public DataArraySchema getPseudoComponentSchema(int i)
    {
        if (i < 0 || i >= pseudoComponentSchemas.size())
            throw new IllegalArgumentException("i < 0 || i >= pseudoComponentSchemas.size()");
        return pseudoComponentSchemas.get(i);
    }

    /**
     * Returns a pseudo-component schema for the given pseudo-component.
     * 
     * @param name pseudo-component name
     * 
     * @return schema
     */
    public DataArraySchema getPseudoComponentSchema(String name)
    {
        for (DataArraySchema pseudocomponentSchema : pseudoComponentSchemas)
            if (name.equals(pseudocomponentSchema.getName()))
                return pseudocomponentSchema;
        return null;
    }

    /**
     * Returns all pseudo-component schemas.
     * 
     * @return	all pseudo-component schemas
     */
    public ArrayList<DataArraySchema> getPseudoComponentSchemas()
    {
        return pseudoComponentSchemas;
    }

    /**
     * Sets the list of component schemas.
     * 
     * @param	componentSchemas list of component schemas, this reference is used internally
     */
    public void setComponentSchemas(ArrayList<DataArraySchema> componentSchemas)
    {
        this.componentSchemas = componentSchemas;
    }

    /**
     * Sets the list of pseudo-component schemas.
     * 
     * @param	pseudoComponentSchemas list of component schemas, this reference is used internally
     */
    public void setPseudoComponentSchemas(ArrayList<DataArraySchema> pseudoComponentSchemas)
    {
        this.pseudoComponentSchemas = pseudoComponentSchemas;
    }

    /**
     * Compares two DataContainerSchemas. Returns true if all components are compatible in this schema and the input schema, false otherwise.
     * 
     * @param	s                    data container to be compared
     * @param	checkComponentNames  flag to include components name checking
     * @param checkComponentRanges flag to include components ranges checking
     * 
     * @return	true if all components are compatible in both schemas, false otherwise.
     */
    public boolean isCompatibleWith(DataContainerSchema s, boolean checkComponentNames, boolean checkComponentRanges)
    {
        if (s == null)
            return false;

        if (componentSchemas.size() != s.getComponentSchemas().size())
            return false;
        for (int i = 0; i < componentSchemas.size(); i++)
            if (!componentSchemas.get(i).isCompatibleWith(s.getComponentSchemas().get(i), checkComponentNames, checkComponentRanges))
                return false;
        return true;
    }

    /**
     * Compares two DataContainerSchemas. Returns true if all components are compatible in this schema and the input schema, false otherwise.
     * 
     * @param	s                   data container to be compared
     * @param	checkComponentNames flag to include components name checking
     * 
     * @return	true if all components are compatible in both schemas, false otherwise.
     */
    public boolean isCompatibleWith(DataContainerSchema s, boolean checkComponentNames)
    {
        return isCompatibleWith(s, checkComponentNames, false);
    }

    /**
     * Compares two DataContainerSchemas. Returns true if all components are compatible in this schema and the input schema, false otherwise.
     * 
     * @param	s data container to be compared
     * 
     * @return	true if all components are compatible in both schemas, false otherwise.
     */
    public boolean isCompatibleWith(DataContainerSchema s)
    {
        //FIXME: should be false but along with OutFieldVisualisationModule.prepareOutputGeometry  
        //(!outField.isStructureCompatibleWith(fieldGeometry.getField());) suspends VisNow
        return isCompatibleWith(s, true);
    }

    /**
     * Returns the name of the container.
     *
     * @return name of the container
     */
    public String getName()
    {
        return name;
    }

    /**
     * Seta the name of the container.
     *
     * @param name new name
     */
    public void setName(String name)
    {
        if (name.isEmpty()) throw new IllegalArgumentException("Data container name cannot be empty");
        this.name = name.replaceAll("\\W", "_");
    }

    /**
     * Returns the user data.
     *
     * @return user data
     */
    public String[] getUserData()
    {
        return userData;
    }

    /**
     * Set the user data.
     *
     * @param userData new user data
     */
    public void setUserData(String[] userData)
    {
        this.userData = userData;
    }

    /**
     * Returns the value of user data at specified index.
     *
     * @param index index
     * 
     * @return the value of user data at specified index
     */
    public String getUserData(int index)
    {
        return this.userData[index];
    }

    /**
     * Set the value of user data at specified index.
     *
     * @param index    index
     * @param userData new value of user data at specified index
     */
    public void setUserData(int index, String userData)
    {
        this.userData[index] = userData;
    }

    /**
     * Returns the number of components.
     * 
     * @return number of components
     */
    public int getNComponents()
    {
        if (componentSchemas == null || componentSchemas.isEmpty())
            return 0;
        return componentSchemas.size();
    }

    /**
     * Returns the schema at specified index.
     * 
     * @param i index
     * 
     * @return schema at specified index
     */
    public DataArraySchema getComponentSchema(int i)
    {
        if (componentSchemas == null || componentSchemas.isEmpty())
            return null;
        if (i < 0 || i >= componentSchemas.size())
            throw new IllegalArgumentException("i < 0 || i >= componentSchemas.size()");
        return componentSchemas.get(i);
    }

    /**
     * Returns the schema with specified name of component.
     * 
     * @param name name of component
     * 
     * @return schema with specified name of component
     */
    public DataArraySchema getComponentSchema(String name)
    {
        if (componentSchemas == null || componentSchemas.isEmpty())
            return null;
        for (DataArraySchema schema : componentSchemas)
            if (schema.getName().equals(name))
                return schema;
        return null;
    }

    /**
     * Returns a deep copy of this instance.
     *
     * @return a deep copy of this instance
     */
    public DataContainerSchema cloneDeep()
    {
        DataContainerSchema clone = new DataContainerSchema(this.name);
        ArrayList<DataArraySchema> componentSchemasClone = new ArrayList<>();
        if (componentSchemas != null && componentSchemas.size() > 0) {
            for (DataArraySchema item : componentSchemas) componentSchemasClone.add(item.cloneDeep());
        }
        clone.componentSchemas = componentSchemasClone;

        ArrayList<DataArraySchema> pseudoComponentSchemasClone = new ArrayList<>();
        if (pseudoComponentSchemas != null && pseudoComponentSchemas.size() > 0) {
            for (DataArraySchema item : pseudoComponentSchemas) pseudoComponentSchemasClone.add(item.cloneDeep());
        }
        clone.pseudoComponentSchemas = pseudoComponentSchemasClone;
        if (userData != null) {
            String[] newUserData = new String[userData.length];
            System.arraycopy(userData, 0, newUserData, 0, newUserData.length);
            clone.setUserData(newUserData);
        }
        return clone;
    }

}
