/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic.cells;

/**
 * Quadrilateral cell.
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
public class Quad extends Cell
{

    /**
     * Constructor of a quadrilateral.
     *  <pre>
     *    i3------- i2
     *     |       / |
     *     |     /   |
     *     |   /     |
     *     | /       |
     *    i0--------i1
     *   </pre>
     * 
     * @param nsp         space dimension
     * @param i0          node indices
     * @param i1          node indices
     * @param i2          node indices
     * @param i3          node indices
     * @param orientation significant if cell is of dim nspace or is a face of a cell of dim nspace
     */
    public Quad(int nsp, int i0, int i1, int i2, int i3, byte orientation)
    {
        type = CellType.QUAD;
        nspace = nsp;
        vertices = new int[4];
        vertices[0] = i0;
        vertices[1] = i1;
        vertices[2] = i2;
        vertices[3] = i3;
        this.orientation = orientation;
        normalize();
    }

   /**
     * Constructor of a quadrilateral.
     *  <pre>
     *    i3------- i2
     *     |       / |
     *     |     /   |
     *     |   /     |
     *     | /       |
     *    i0--------i1
     *   </pre>
     * 
     * @param nsp         space dimension
     * @param i0          node indices
     * @param i1          node indices
     * @param i2          node indices
     * @param i3          node indices
     */
    public Quad(int nsp, int i0, int i1, int i2, int i3)
    {
        this(nsp, i0, i1, i2, i3, (byte) 1);
    }

    @Override
    public Cell[][] subcells()
    {
        Cell[][] tr = new Cell[3][];
        tr[2] = new Quad[1];
        tr[2][0] = this;
        tr[1] = faces();
        tr[0] = new Point[vertices.length];
        for (int i = 0; i < vertices.length; i++)
            tr[0][i] = new Point(nspace, vertices[i]);
        return tr;
    }

    @Override
    public Cell[] triangulation()
    {
        Triangle[] subdiv = {new Triangle(nspace, vertices[0], vertices[1], vertices[2], orientation),
                             new Triangle(nspace, vertices[0], vertices[2], vertices[3], orientation)};
        return subdiv;
    }

    @Override
    public int[][] triangulationVertices()
    {
        return new int[][]{{vertices[0], vertices[1], vertices[2]},
                           {vertices[0], vertices[2], vertices[3]}};
    }

    /**
     * Generates the array of triangle vertices from the array of quadrilateral
     * vertices.
     *
     * @param vertices array of vertices
     *
     * @return array of triangle vertices
     */
    public static int[][] triangulationVertices(int[] vertices)
    {
        return new int[][]{{vertices[0], vertices[1], vertices[2]},
                           {vertices[0], vertices[2], vertices[3]}};
    }

   /**
     * Generates the array of triangles in boundary triangulation of the
     * quadrilateral.
     *
     * @param vertices array of vertices
     * @return array of triangles in boundary triangulation
     */
    public static int[][] triangulationIndices(int[] vertices)
    {
        return new int[][]{{0, 1, 2}, {0, 2, 3}};
    }

    @Override
    public Cell[] faces(int[] nodes, byte orientation)
    {
        Segment[] faces = {new Segment(nspace, nodes[0], nodes[1], orientation),
                           new Segment(nspace, nodes[1], nodes[2], orientation),
                           new Segment(nspace, nodes[2], nodes[3], orientation),
                           new Segment(nspace, nodes[3], nodes[0], orientation)};
        return faces;
    }

    @Override
    public Cell[] faces()
    {
        return faces(vertices, orientation);
    }

    @Override
    public final int[] normalize()
    {
        if (!normalize(vertices))
            orientation = (byte) (1 - orientation);
        return vertices;
    }

    /**
     * A convenience function that reorders array of vertices to increasing order (as
     * long as possible).
     *
     * @param vertices vertices
     *
     * @return true if orientation has been preserved, false otherwise
     */
    public static boolean normalize(int[] vertices)
    {
        int i = 0, t;
        for (int j = 1; j < 4; j++)
            if (vertices[j] < vertices[i])
                i = j;
        switch (i) {
            case 0:
                break;
            case 1:
                t = vertices[0];
                vertices[0] = vertices[1];
                vertices[1] = vertices[2];
                vertices[2] = vertices[3];
                vertices[3] = t;
                break;
            case 2:
                t = vertices[0];
                vertices[0] = vertices[2];
                vertices[2] = t;
                t = vertices[1];
                vertices[1] = vertices[3];
                vertices[3] = t;
                break;
            case 3:
                t = vertices[0];
                vertices[0] = vertices[3];
                vertices[3] = vertices[2];
                vertices[2] = vertices[1];
                vertices[1] = t;
                break;
            default:
                throw new IllegalArgumentException("Invalid index " + i);
        }
        if (vertices[1] > vertices[3]) {
            t = vertices[3];
            vertices[3] = vertices[1];
            vertices[1] = t;
            return false;
        }
        return true;
    }

    @Override
    public byte compare(int[] v)
    {
        if (v.length != 4)
            return 0;
        return compare(new Quad(this.nspace, v[0], v[1], v[2], v[3]));
    }

}
