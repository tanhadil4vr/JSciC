/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */

package pl.edu.icm.jscic.cells;

import static org.apache.commons.math3.util.FastMath.*;
import static pl.edu.icm.jscic.cells.CellType.*;
import pl.edu.icm.jlargearrays.FloatLargeArray;

/**
 * An abstract representation of a cell object.
 *
 * @author Krzysztof S. Nowinski, University of Warsaw, ICM
 */
abstract public class Cell
{

    private static final int[][] ORIENTING_VERTS
        = {{0}, {0}, {0}, {0}, {0, 1, 2, 3}, {0, 1, 3, 4}, {0, 1, 2, 3}, {0, 1, 3, 4}};

    protected CellType type;
    protected int nspace;
    protected int[] vertices;
    protected byte orientation;

    /**
     * Returns cell face corresponding to a given cell type.
     * 
     * @param type cell type
     * 
     * @return cell face
     */
    public static int[] cellTypeToFaceType(CellType type)
    {
        switch (type) {
            case EMPTY:
                return null;
            case POINT:
                return new int[]{0, 0, 0, 0, 0, 0, 0, 0};
            case SEGMENT:
                return new int[]{2, 0, 0, 0, 0, 0, 0, 0};
            case TRIANGLE:
                return new int[]{0, 3, 0, 0, 0, 0, 0, 0};
            case QUAD:
                return new int[]{0, 4, 0, 0, 0, 0, 0, 0};
            case TETRA:
                return new int[]{0, 0, 4, 0, 0, 0, 0, 0};
            case PYRAMID:
                return new int[]{0, 0, 4, 1, 0, 0, 0, 0};
            case PRISM:
                return new int[]{0, 0, 2, 3, 0, 0, 0, 0};
            case HEXAHEDRON:
                return new int[]{0, 0, 0, 6, 0, 0, 0, 0};
            case EXPLICIT:
                return null;
            case CUSTOM:
                return null;
            default:
                return null;
        }
    }

    /**
     * Returns all valid cell types.
     * 
     * @return valid cell types
     */
    public static CellType[] getProperCellTypes()
    {
        return new CellType[]{POINT, SEGMENT, TRIANGLE, QUAD, TETRA, PYRAMID, PRISM, HEXAHEDRON};
    }

    /**
     * Returns the number of valid cell types.
     * 
     * @return the number of valid cell types
     */
    public static int getNProperCellTypes()
    {
        return getProperCellTypes().length;
    }

    /**
     * Returns all valid 1D cell types.
     * 
     * @return all valid 1D cell types
     */
    public static CellType[] getProperCellTypesUpto1D()
    {
        return new CellType[]{POINT, SEGMENT};
    }

    /**
     * Returns the number of valid 1D cell types.
     * 
     * @return the number of valid 1D cell types
     */
    public static int getNProperCellTypesUpto1D()
    {
        return getProperCellTypesUpto1D().length;
    }

    /**
     * Returns all valid 1D and 2D cell types.
     * 
     * @return all valid 1D and 2D cell types
     */
    public static CellType[] getProperCellTypesUpto2D()
    {
        return new CellType[]{POINT, SEGMENT, TRIANGLE, QUAD};
    }

    /**
     * Returns the number of valid 1D and 2D cell types.
     * 
     * @return the number of valid 1D and 2D cell types
     */
    public static int getNProperCellTypesUpto2D()
    {
        return getProperCellTypesUpto2D().length;
    }

    /**
     * Creates a new cell object.
     *
     * @param type        the type of cell
     * @param nspace      the nspace of a cell
     * @param vertices    the coordinates of vertices
     * @param orientation the orientation of a cell
     *
     * @return a new cell object
     *
     */
    public static Cell createCell(CellType type, int nspace, int[] vertices, byte orientation)
    {
        if (vertices.length != type.getNVertices() || nspace < type.getDim())
            throw new IllegalArgumentException("vertices.length != type.getNVertices() || nspace < type.getDim()");
        switch (type) {
            case POINT:
                return new Point(nspace, vertices[0], orientation);
            case SEGMENT:
                return new Segment(nspace, vertices[0], vertices[1], orientation);
            case TRIANGLE:
                return new Triangle(nspace, vertices[0], vertices[1], vertices[2], orientation);
            case QUAD:
                return new Quad(nspace, vertices[0], vertices[1], vertices[2], vertices[3], orientation);
            case TETRA:
                return new Tetra(nspace, vertices[0], vertices[1], vertices[2], vertices[3], orientation);
            case PYRAMID:
                return new Pyramid(nspace, vertices[0], vertices[1], vertices[2], vertices[3], vertices[4], orientation);
            case PRISM:
                return new Prism(nspace, vertices[0], vertices[1], vertices[2], vertices[3], vertices[4], vertices[5], orientation);
            case HEXAHEDRON:
                return new Hex(nspace, vertices[0], vertices[1], vertices[2], vertices[3], vertices[4], vertices[5], vertices[6], vertices[7], orientation);
            default:
                throw new IllegalArgumentException("Invalid cell type.");
        }
    }

    /**
     * Creates a new cell object.
     *
     * @param type        the type of cell
     * @param vertices    the coordinates of vertices
     * @param orientation the orientation of a cell
     *
     * @return a new cell object
     */
    public static Cell createCell(CellType type, int[] vertices, byte orientation)
    {
        if (vertices.length != type.getNVertices())
            throw new IllegalArgumentException("vertices.length != type.getNVertices()");
        int nspace = type.getDim();
        switch (type) {
            case POINT:
                return new Point(nspace, vertices[0], orientation);
            case SEGMENT:
                return new Segment(nspace, vertices[0], vertices[1], orientation);
            case TRIANGLE:
                return new Triangle(nspace, vertices[0], vertices[1], vertices[2], orientation);
            case QUAD:
                return new Quad(nspace, vertices[0], vertices[1], vertices[2], vertices[3], orientation);
            case TETRA:
                return new Tetra(nspace, vertices[0], vertices[1], vertices[2], vertices[3], orientation);
            case PYRAMID:
                return new Pyramid(nspace, vertices[0], vertices[1], vertices[2], vertices[3], vertices[4], orientation);
            case PRISM:
                return new Prism(nspace, vertices[0], vertices[1], vertices[2], vertices[3], vertices[4], vertices[5], orientation);
            case HEXAHEDRON:
                return new Hex(nspace, vertices[0], vertices[1], vertices[2], vertices[3], vertices[4], vertices[5], vertices[6], vertices[7], orientation);
            default:
                throw new IllegalArgumentException("Invalid cell type.");
        }
    }

    /**
     * Returns a deep copy of this instance. 
     *
     * @return a deep copy of this instance
     */
    public Cell cloneDeep()
    {
        return createCell(type, nspace, vertices, orientation);
    }

    /**
     * Returns cell type.
     * 
     * @return cell type
     */
    public CellType getType()
    {
        return type;
    }

    /**
     * Returns true if the cell is simplex.
     * 
     * @return true if the cell is simplex, false otherwise.
     */
    boolean isSimplex()
    {
        return type.isSimplex();
    }

    /**
     * Returns the dimension of the cell.
     * 
     * @return the dimension of the cell.
     */
    public int getDim()
    {
        return type.getDim();
    }

    /**
     * Returns the nspace of the cell.
     * 
     * @return the nspace of the cell.
     */
    public int getNspace()
    {
        return nspace;
    }

    /**
     * Returns the number of vertices of the cell.
     * 
     * @return the number of vertices of the cell
     */
    public int getNVertices()
    {
        return type.getNVertices();
    }

    /**
     * Returns the vertices of the cell.
     * 
     * @return the vertices of the cell
     */
    public int[] getVertices()
    {
        return vertices;
    }

    /**
     * Returns the orientation of the cell.
     * 
     * @return the orientation of the cell
     */
    public byte getOrientation()
    {
        return orientation;
    }

    /**
     * Sets the orientation of the cell.
     * 
     * @param orientation the orientation
     */
    public void setOrientation(byte orientation)
    {
        this.orientation = orientation == 0 ? (byte) 0 : (byte) 1;
    }

    @Override
    public String toString()
    {
        StringBuffer b = new StringBuffer();
        b.append("[");
        for (int i = 0; i < vertices.length; i++)
            b.append(String.format("%5d ", vertices[i]));
        b.append("]");
        if (orientation == 1)
            b.append(" +");
        else
            b.append(" -");
        return new String(b);
    }

    /**
     * Creates complete list of subcells (faces, edges, vertices).
     * 
     * @return array of length (dim+1) of arrays of cells
     *         (Cells[i] is array of subcells of dimension i)
     */
    abstract public Cell[][] subcells();

    /**
     * Creates array of cell faces.
     * 
     * @return array faces
     */
    abstract public Cell[] faces();

    /**
     * Creates array of cell faces for a given set of node indices and orientation.
     * 
     * @param nodes       nodes
     * @param orientation orientation
     * 
     * @return array faces
     */
    abstract public Cell[] faces(int[] nodes, byte orientation);

    /**
     * Divides into array of simplices.
     * 
     * @return array of simplices of triangulation
     */
    abstract public Cell[] triangulation();

    /**
     * Divides into array of simplices.
     * 
     * @return array of simplices of triangulation
     */
    abstract public int[][] triangulationVertices();

    /**
     * Reorders vertices to increasing order (as long as possible) modifying orientation if necessary.
     * 
     * @return
     *         array of reordered vertices
     */
    abstract protected int[] normalize();

    /**
     * Returns a byte describing compatibility with x.
     * 
     * @param x compared cell
     * 
     * @return
     *         
     * 0 if x is of different dimension
     * 
     * 0 if x has no common face
     * 
     * 1 if x has the same type, vertices and orientation
     * 
     * -1 if x has the same type and vertices but opposite orientation
     * 
     * 2 if x has common face and compatible orientation
     * 
     * -2 if x has common face but opposite orientation
     * 
     * 3 if x is a compatibly oriented face of this
     * 
     * -3 if x is a oppositely oriented face of this
     * 
     * 4 if this is a compatibly oriented face of x
     * 
     * -4 if this is a oppositely oriented face of x
     */
    public byte compare(Cell x)
    {
        Cell[] f;
        Cell[] xf;
        if (x.getDim() == this.getDim()) {
            if (x.getNVertices() == this.getNVertices()) {
                int[] v = x.getVertices();
                boolean eq = true;
                for (int i = 0; i < v.length; i++)
                    if (v[i] != vertices[i]) {
                        eq = false;
                        break;
                    }
                if (eq && (x.getOrientation() == orientation))
                    return 1;
                else if (eq)
                    return -1;
            }
            f = this.faces();
            xf = x.faces();
            if (f != null && xf != null)
                for (Cell f1 : f)
                    for (Cell xf1 : xf) {
                        if (f1.compare(xf1) == 1)
                            return -2;
                        if (f1.compare(xf1) == -1)
                            return 2;
                    }
            return 0;
        }
        if (x.getDim() == this.getDim() - 1) {
            f = this.faces();
            if (f != null)
                for (Cell f1 : f) {
                    if (f1.compare(x) == 1)
                        return 3;
                    if (f1.compare(x) == -1)
                        return -3;
                }
            return 0;
        }
        if (x.getDim() == this.getDim() + 1) {
            xf = x.faces();
            if (xf != null)
                for (Cell xf1 : xf) {
                    if (xf1.compare(this) == 1)
                        return 4;
                    if (xf1.compare(this) == -1)
                        return -4;
                }
            return 0;
        }
        return 0;
    }

    /**
     * Returns a byte describing compatibility with x of a cell of the same type with vertices v.
     * 
     * @param v array of vertex numbers
     * 
     * @return
     *         
     * 0 if length of v is different from number of vertices of this
     * 
     * 0 if x has no common face
     * 
     * 1 if x has the same type, vertices and orientation
     * 
     * -1 if x has the same type and vertices but opposite orientation
     * 
     * 2 if x has common face and compatible orientation
     * 
     * -2 if x has common face but opposite orientation
     */
    abstract public byte compare(int[] v);

    /**
     * Returns geometric orientation of the cell.
     * 
     * @param coords vertex coordinates
     * @param nodes  list of nodes
     * 
     * @return orientation of the cell
     *
     */
    public int geomOrientation(FloatLargeArray coords, int[] nodes)
    {
        int[] orv = ORIENTING_VERTS[type.getValue()];
        if (orv.length < 4 || nodes.length < 4)
            return 0;
        int[] verts = new int[4];
        for (int i = 0; i < verts.length; i++)
            verts[i] = nodes[orv[i]];
        float[][] v = new float[nspace][nspace];
        float d = 0;
        for (int i = 0; i < nspace; i++)
            for (int j = 0; j < nspace; j++)
                v[i][j] = coords.getFloat(nspace * (long) verts[i + 1] + j) - coords.getFloat(nspace * (long) verts[0] + j);
        if (nspace == 1) {
            d = v[0][0];
        } else if (nspace == 2) {
            d = v[0][0] * v[1][1] - v[0][1] * v[1][0];
        } else if (nspace == 3) {
            d = v[0][0] * v[1][1] * v[2][2] + v[0][1] * v[1][2] * v[2][0] + v[0][2] * v[1][0] * v[2][1] -
                v[0][1] * v[1][0] * v[2][2] - v[0][0] * v[1][2] * v[2][1] - v[0][2] * v[1][1] * v[2][0];
        }
        return (int) (signum(d));
    }

    /**
     * Returns geometric orientation of the cell.
     * 
     * @param coords vertex coordinates
     * 
     * @return orientation of the cell
     */
    public int geomOrientation(FloatLargeArray coords)
    {
        int[] orv = ORIENTING_VERTS[type.getValue()];
        if (orv.length < 4 || vertices.length < 4)
            return 0;
        int[] verts = new int[4];
        for (int i = 0; i < verts.length; i++)
            verts[i] = vertices[orv[i]];
        float[][] v = new float[nspace][nspace];
        float d;
        for (int i = 0; i < nspace; i++)
            for (int j = 0; j < nspace; j++)
                v[i][j] = coords.getFloat(nspace * (long) verts[i + 1] + j) - coords.getFloat(nspace * (long) verts[0] + j);
        if (nspace == 2)
            d = v[0][0] * v[1][1] - v[0][1] * v[1][0];
        else
            d = v[0][0] * v[1][1] * v[2][2] + v[0][1] * v[1][2] * v[2][0] + v[0][2] * v[1][0] * v[2][1] -
                v[0][1] * v[1][0] * v[2][2] - v[0][0] * v[1][2] * v[2][1] - v[0][2] * v[1][1] * v[2][0];
        return (int) (signum(d));
    }

    /**
     * Returns geometric orientation of the cell.
     * 
     * @param type      cell type
     * @param coords    vertex coordinates
     * @param geomspace geometric space
     * @param vertices  vertices
     * @param nspace    nspace
     * 
     * @return orientation of the cell
     */
    public static int geomOrientation(CellType type, int nspace, int geomspace, int[] vertices, FloatLargeArray coords)
    {
        int[] orv = ORIENTING_VERTS[type.getValue()];
        if (orv.length < 4 || vertices.length < 4)
            return 0;
        int[] verts = new int[4];
        for (int i = 0; i < verts.length; i++)
            verts[i] = vertices[orv[i]];
        float[][] v = new float[geomspace][geomspace];
        float d;
        for (int i = 0; i < geomspace; i++)
            for (int j = 0; j < geomspace; j++)
                v[i][j] = coords.getFloat(nspace * (long) verts[i + 1] + j) - coords.getFloat(nspace * (long) verts[0] + j);
        if (geomspace == 2)
            d = v[0][0] * v[1][1] - v[0][1] * v[1][0];
        else
            d = v[0][0] * v[1][1] * v[2][2] + v[0][1] * v[1][2] * v[2][0] + v[0][2] * v[1][0] * v[2][1] -
                v[0][1] * v[1][0] * v[2][2] - v[0][0] * v[1][2] * v[2][1] - v[0][2] * v[1][1] * v[2][0];
        return (int) (signum(d));
    }

    /**
     * Returns geometric orientation of the cell.
     * 
     * @param coords vertex coordinates
     * @param normal vector normal to the surface
     * 
     * @return orientation of the cell
     */
    public int geomOrientation(FloatLargeArray coords, float[] normal)
    {
        if (vertices.length < 3 || normal == null || vertices[0] < 0 || vertices[1] < 0 || vertices[2] < 0)
            return 0;
        float[][] v = new float[nspace][nspace];
        float d = 0;
        for (int i = 0; i < nspace - 1; i++)
            for (int j = 0; j < nspace; j++)
                v[i][j] = coords.getFloat(nspace * (long) vertices[i + 1] + j) - coords.getFloat(nspace * (long) vertices[0] + j);
        System.arraycopy(normal, 0, v[nspace - 1], 0, nspace);
        if (nspace == 1) {
            d = v[0][0];
        } else if (nspace == 2) {
            d = v[0][0] * v[1][1] - v[0][1] * v[1][0];
        } else if (nspace == 3) {
            d = v[0][0] * v[1][1] * v[2][2] + v[0][1] * v[1][2] * v[2][0] + v[0][2] * v[1][0] * v[2][1] -
                v[0][1] * v[1][0] * v[2][2] - v[0][0] * v[1][2] * v[2][1] - v[0][2] * v[1][1] * v[2][0];
        }
        return (int) (signum(d));
    }

    /**
     * Utility subdividing a quad according to the convention of lowest vertex at the diagonal.
     * 
     * @param quad numbers of four quad vertices (ordered as in Quad class)
     * 
     * @return array describing two triangles of quad triangulation
     */
    protected static int[][] diagonalSubdiv(int[] quad)
    {
        if (quad == null || quad.length != 4)
            throw new IllegalArgumentException("quad == null || quad.length != 4");
        if ((quad[0] < quad[1] && quad[0] < quad[3]) ||
            (quad[2] < quad[1] && quad[2] < quad[3]))
            return new int[][]{{quad[0], quad[1], quad[2]}, {quad[0], quad[2], quad[3]}};
        else
            return new int[][]{{quad[1], quad[2], quad[3]}, {quad[1], quad[3], quad[0]}};
    }

    /**
     * Utility subdividing a quad according to the convention of lowest vertex at the diagonal.
     * 
     * @param quad numbers of four quad vertices (ordered as in Quad class)
     * 
     * @return array describing two triangles of quad triangulation
     */
    protected static int[][] diagonalSubdivIndices(int[] quad)
    {
        if (quad == null || quad.length != 4)
            throw new IllegalArgumentException("quad == null || quad.length != 4");
        if ((quad[0] < quad[1] && quad[0] < quad[3]) ||
            (quad[2] < quad[1] && quad[2] < quad[3]))
            return new int[][]{{0, 1, 2}, {0, 2, 3}};
        else
            return new int[][]{{1, 2, 3}, {1, 3, 0}};
    }

    /**
     * Returns the measure of the cell (length, area, volume).
     * 
     * @param coords coordinates
     * 
     * @return the measure of the cell
     */
    public float getMeasure(FloatLargeArray coords)
    {
        long i0, i1, i2, i3;
        float[] v1 = new float[3];
        float[] v2 = new float[3];
        float[] v3 = new float[3];
        float r;
        switch (type) {
            case POINT:
                return 1;
            case SEGMENT:
                r = 0;
                i1 = vertices[1];
                i0 = vertices[0];
                for (int i = 0; i < 3; i++)
                    r += (coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i)) * (coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i));
                return (float) (sqrt(r));
            case TRIANGLE:
                r = 0;
                i2 = vertices[2];
                i1 = vertices[1];
                i0 = vertices[0];
                for (int i = 0; i < 3; i++) {
                    v1[i] = coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i);
                    v2[i] = coords.getFloat(3 * i2 + i) - coords.getFloat(3 * i0 + i);
                }
                v3[0] = v1[1] * v2[2] - v2[1] * v1[2];
                v3[1] = v1[2] * v2[0] - v2[2] * v1[0];
                v3[2] = v1[0] * v2[1] - v2[0] * v1[1];
                for (int i = 0; i < 3; i++)
                    r += v3[i] * v3[i];
                return (float) (sqrt(r)) / 2;
            case TETRA:
                r = 0;
                i3 = vertices[3];
                i2 = vertices[2];
                i1 = vertices[1];
                i0 = vertices[0];
                for (int i = 0; i < 3; i++) {
                    v1[i] = coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i);
                    v2[i] = coords.getFloat(3 * i2 + i) - coords.getFloat(3 * i0 + i);
                    v3[i] = coords.getFloat(3 * i3 + i) - coords.getFloat(3 * i0 + i);
                }
                r = v1[0] * v2[1] * v3[2] + v1[1] * v2[2] * v3[0] + v1[2] * v2[0] * v3[1] -
                    (v1[2] * v2[1] * v3[0] + v1[0] * v2[2] * v3[1] + v1[1] * v2[0] * v3[2]);
                return abs(r) / 6;
            default:
                Cell[] triang = triangulation();
                r = 0;
                for (Cell triang1 : triang)
                    r += triang1.getMeasure(coords);
                return r;
        }
    }

    /**
     * Returns the signed measure of the cell (length, area, volume).
     * In case of 3D cells, oriented volume is returned.
     * 
     * @param coords coordinates
     * 
     * @return the signed measure of the cell
     */
    public float getSignedMeasure(FloatLargeArray coords)
    {
        long i0, i1, i2, i3;
        float[] v1 = new float[3];
        float[] v2 = new float[3];
        float[] v3 = new float[3];
        float r;
        switch (type) {
            case POINT:
                return 1;
            case SEGMENT:
                r = 0;
                i1 = vertices[1];
                i0 = vertices[0];
                for (int i = 0; i < 3; i++)
                    r += (coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i)) * (coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i));
                return (float) (sqrt(r));
            case TRIANGLE:
                r = 0;
                i2 = vertices[2];
                i1 = vertices[1];
                i0 = vertices[0];
                for (int i = 0; i < 3; i++) {
                    v1[i] = coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i);
                    v2[i] = coords.getFloat(3 * i2 + i) - coords.getFloat(3 * i0 + i);
                }
                v3[0] = v1[1] * v2[2] - v2[1] * v1[2];
                v3[1] = v1[2] * v2[0] - v2[2] * v1[0];
                v3[2] = v1[0] * v2[1] - v2[0] * v1[1];
                for (int i = 0; i < 3; i++)
                    r += v3[i] * v3[i];
                return (float) (sqrt(r)) / 2;
            case TETRA:
                r = 0;
                i3 = vertices[3];
                i2 = vertices[2];
                i1 = vertices[1];
                i0 = vertices[0];
                for (int i = 0; i < 3; i++) {
                    v1[i] = coords.getFloat(3 * i1 + i) - coords.getFloat(3 * i0 + i);
                    v2[i] = coords.getFloat(3 * i2 + i) - coords.getFloat(3 * i0 + i);
                    v3[i] = coords.getFloat(3 * i3 + i) - coords.getFloat(3 * i0 + i);
                }
                r = v1[0] * v2[1] * v3[2] + v1[1] * v2[2] * v3[0] + v1[2] * v2[0] * v3[1] -
                    (v1[2] * v2[1] * v3[0] + v1[0] * v2[2] * v3[1] + v1[1] * v2[0] * v3[2]);
                float sign = this.orientation == 1 ? 1 : -1;
                return sign * r / 6;
            default:
                Cell[] triang = triangulation();
                r = 0;
                for (Cell triang1 : triang)
                    r += triang1.getSignedMeasure(coords);
                return r;
        }
    }
}
