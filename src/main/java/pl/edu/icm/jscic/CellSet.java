/* ***** BEGIN LICENSE BLOCK *****
 * Java Scientific Containers (JSciC)
 * Copyright (C) 2015 onward University of Warsaw, ICM
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ***** END LICENSE BLOCK ***** */
package pl.edu.icm.jscic;

import java.util.ArrayList;
import pl.edu.icm.jscic.cells.Cell;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jlargearrays.LargeArray;

/**
 * Collection of cell arrays spanning some area in space.
 *
 * @author Krzysztof S. Nowinski
 *
 * University of Warsaw, ICM
 */
public class CellSet extends DataContainer {

    private int nCells;
    private CellArray[] cellArrays = new CellArray[Cell.getNProperCellTypes()];
    private CellArray[] boundaryCellArrays = new CellArray[Cell.getNProperCellTypesUpto2D()];
    private int nActiveNodes = -1;
    private int nDataValues = -1;
    private boolean selected = false;

    /**
     * Creates a new instance of CellSet.
     */
    public CellSet() {
        this("cell set");
    }

    /**
     * Creates a new instance of CellSet.
     *
     * @param name name of the CellSet
     */
    public CellSet(String name) {
        schema = new DataContainerSchema(name);
        for (int i = 0; i < cellArrays.length; i++) {
            cellArrays[i] = null;
        }
        for (int i = 0; i < boundaryCellArrays.length; i++) {
            boundaryCellArrays[i] = null;
        }
        timestamp = System.nanoTime();
    }

    /**
     * Creates a new instance of CellSet.
     *
     * @param schema schema of the CellSet
     */
    public CellSet(DataContainerSchema schema) {
        this.schema = schema;
        for (int i = 0; i < cellArrays.length; i++) {
            cellArrays[i] = null;
        }
        for (int i = 0; i < boundaryCellArrays.length; i++) {
            boundaryCellArrays[i] = null;
        }
        timestamp = System.nanoTime();
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(schema.getName()).append(":   ");
        for (int i = 0; i < Cell.getNProperCellTypes(); i++) {
            if (cellArrays[i] != null) {
                CellArray ca = cellArrays[i];
                s.append("").append(ca.getNCells()).append(" ").append(ca.getType().getUCDName()).append("s ");
            }
        }
        s.append("\nboundary:   ");
        for (int i = 0; i < Cell.getNProperCellTypesUpto2D(); i++) {
            if (boundaryCellArrays[i] != null) {
                CellArray ca = boundaryCellArrays[i];
                s.append("").append(ca.getNCells()).append(" ").append(ca.getType().getUCDName()).append("s ");
            }
        }

        if (components.size() > 0) {
            for (DataArray dataArray : components) {
                s.append(dataArray.toString()).append("\n");
            }
        } else {
            s.append("\n");
        }
        return s.toString();
    }

    /**
     * Returns the description of the CellSet.
     *
     * @return description of the CellSet
     */
    public String description() {
        return description(false);
    }

    /**
     * Returns the description of the CellSet.
     *
     * @param debug if true, then an extended description is returned
     *
     * @return description of the CellSet
     */
    public String description(boolean debug) {
        StringBuilder s = new StringBuilder();
        s.append(schema.getName()).append(":   ");
        for (int i = 0; i < Cell.getNProperCellTypes(); i++) {
            if (cellArrays[i] != null) {
                CellArray ca = cellArrays[i];
                s.append("").append(ca.getNCells()).append(" ").append(ca.getType().getUCDName()).append("s ");
            }
        }
        s.append("<br>boundary:   ");
        for (int i = 0; i < Cell.getNProperCellTypesUpto2D(); i++) {
            if (boundaryCellArrays[i] != null) {
                CellArray ca = boundaryCellArrays[i];
                s.append("").append(ca.getNCells()).append(" ").append(ca.getType().getUCDName()).append("s ");
            }
        }

        if (components.size() > 0) {
            s.append("<br>Cell data components:");
            s.append("<TABLE border=\"0\" cellspacing=\"5\">");
            if (debug) {
                s.append("<TR valign='top' align='right'><TD align = 'left'>Name</td><TD>Vector<br/>length</td><td>Type</td><td>Time<br/>steps</td><td>Min</td><td>Max</td><td>Physical<br/>min</td><td>Physical<br/>max</td><td>Preferred<br/>min</td><td>Preferred<br/>max</td><td>Preferred<br/>physical<br/>min</td><td>Preferred<br/>physical<br/>max</td></tr>");
            } else {
                s.append("<TR valign='top' align='right'><TD align = 'left'>Name</td><TD>Vector<br/>length</td><td>Type</td><td>Time<br/>steps</td><td>Min</td><td>Max</td><td>Physical<br/>min</td><td>Physical<br/>max</td></tr>");
            }
            for (DataArray dataArray : components) {
                s.append(dataArray.description(debug)).append("<br>");
            }
            s.append("</table>");
        } else {
            s.append("<br>");
        }
        return s.toString();
    }

    @Override
    public CellSet cloneShallow() {
        ArrayList<DataArray> dataClone = new ArrayList<>();
        if (components != null && components.size() > 0) {
            for (DataArray dataElem : components) {
                dataClone.add(dataElem.cloneShallow());
            }
        }
        CellArray[] cellArraysClone = null;
        if (cellArrays != null && cellArrays.length > 0) {
            cellArraysClone = new CellArray[cellArrays.length];
            for (int i = 0; i < cellArrays.length; i++) {
                cellArraysClone[i] = cellArrays[i] != null ? cellArrays[i].cloneShallow() : null;
            }
        }
        CellArray[] boundaryCellArraysClone = null;
        if (boundaryCellArrays != null && boundaryCellArrays.length > 0) {
            boundaryCellArraysClone = new CellArray[boundaryCellArrays.length];
            for (int i = 0; i < boundaryCellArrays.length; i++) {
                boundaryCellArraysClone[i] = boundaryCellArrays[i] != null ? boundaryCellArrays[i].cloneShallow() : null;
            }
        }
        CellSet clone = new CellSet(schema.cloneDeep());
        clone.nElements = nElements;
        clone.setComponents(dataClone);
        clone.setCurrentFrame(currentFrame);
        clone.setCurrentTime(currentTime);
        clone.setCellArrays(cellArraysClone);
        clone.setBoundaryCellArrays(boundaryCellArraysClone);
        clone.setNCells(nCells);
        clone.nActiveNodes = nActiveNodes;
        clone.setNDataValues(nDataValues);
        clone.setSelected(selected);
        clone.timestamp = timestamp;
        return clone;
    }

    @Override
    public CellSet cloneDeep() {
        ArrayList<DataArray> dataClone = new ArrayList<>();
        if (components != null && components.size() > 0) {
            for (DataArray dataElem : components) {
                dataClone.add(dataElem.cloneDeep());
            }
        }
        CellArray[] cellArraysClone = null;
        if (cellArrays != null && cellArrays.length > 0) {
            cellArraysClone = new CellArray[cellArrays.length];
            for (int i = 0; i < cellArrays.length; i++) {
                cellArraysClone[i] = cellArrays[i] != null ? cellArrays[i].cloneDeep() : null;
            }
        }
        CellArray[] boundaryCellArraysClone = null;
        if (boundaryCellArrays != null && boundaryCellArrays.length > 0) {
            boundaryCellArraysClone = new CellArray[boundaryCellArrays.length];
            for (int i = 0; i < boundaryCellArrays.length; i++) {
                boundaryCellArraysClone[i] = boundaryCellArrays[i] != null ? boundaryCellArrays[i].cloneDeep() : null;
            }
        }
        CellSet clone = new CellSet(schema.cloneDeep());
        clone.nElements = nElements;
        clone.setComponents(dataClone);
        clone.setCurrentFrame(currentFrame);
        clone.setCurrentTime(currentTime);
        clone.setCellArrays(cellArraysClone);
        clone.setBoundaryCellArrays(boundaryCellArraysClone);
        clone.setNCells(nCells);
        clone.nActiveNodes = nActiveNodes;
        clone.setNDataValues(nDataValues);
        clone.setSelected(selected);
        clone.timestamp = this.timestamp;
        return clone;
    }

    /**
     * Returns total number of cells of all types.
     *
     * @return number of cells
     */
    public int getNCells() {
        return nCells;
    }

    /**
     * Sets total number of cells.
     *
     * @param nCells the new number of cells
     */
    public void setNCells(int nCells) {
        this.nCells = nCells;
    }

    /**
     * Returns the number of data values. It may be different than the number of
     * cells.
     *
     * @return number of data values
     */
    public int getNDataValues() {
        return nDataValues;
    }

    /**
     * Sets the number of data values.
     *
     * @param nDataValues number of data values
     */
    public void setNDataValues(int nDataValues) {
        this.nDataValues = nDataValues;
        timestamp = System.nanoTime();
    }

    /**
     * Returns an array of CellArrays. This method always returns a reference to
     * the internal array.
     *
     * @return array of CellArrays
     */
    public CellArray[] getCellArrays() {
        return cellArrays;
    }

    /**
     * Sets the array of CellArrays.
     *
     * @param cellArrays array of CellArrays, this reference is used internally
     * (i.e. the array is not cloned)
     */
    public void setCellArrays(CellArray[] cellArrays) {
        if (cellArrays.length != Cell.getNProperCellTypes()) {
            throw new IllegalArgumentException("cellArrays.length != Cell.getNProperCellTypes()");
        }
        this.cellArrays = cellArrays;
        nCells = 0;
        for (CellArray cellArray : cellArrays) {
            if (cellArray != null) {
                nCells += cellArray.getNCells();
            }
        }
        for (int i = 0; i < CellType.TETRA.getValue(); i++) {
            CellArray cellArray = cellArrays[i];
            if (cellArray != null && (boundaryCellArrays[i] == null || boundaryCellArrays[i].getNCells() == 0)) {
                boundaryCellArrays[i] = cellArray;
            }
        }
        timestamp = System.nanoTime();
    }

    /**
     * Returns the CellArray of a specified type. This method always returns a
     * reference to the internal CellArray.
     *
     * @param type type of CellArray
     *
     * @return CellArray of a specified type
     */
    public CellArray getCellArray(CellType type) {
        if (type.getValue() < 0 || type.getValue() >= Cell.getNProperCellTypes()) {
            throw new IllegalArgumentException("type.getValue() < 0 || type.getValue() >= Cell.getNProperCellTypes()");
        }
        return this.cellArrays[type.getValue()];
    }

    /**
     * Sets the new CellArray.
     *
     * @param newCellArray new CellArray at specified index (==
     * newCellArray.getType().getValue()), this reference is used internally
     * (i.e. the CellArray is not cloned)
     */
    public void setCellArray(CellArray newCellArray) {
        if (newCellArray == null) {
            return;
        }
        cellArrays[newCellArray.getType().getValue()] = newCellArray;
        if (newCellArray.getType().getValue() <= CellType.QUAD.getValue()) {
            int i = newCellArray.getType().getValue();
            if (boundaryCellArrays[i] == null || boundaryCellArrays[i].getNCells() == 0) {
                boundaryCellArrays[i] = newCellArray;
            }
        }
        nCells = 0;
        for (CellArray cellArray : cellArrays) {
            if (cellArray != null) {
                nCells += cellArray.getNCells();
            }
        }
        timestamp = System.nanoTime();
    }

    /**
     * Returns the boundary CellArrays. Boundary cells are 0,1, and
     * 2-dimensional cells of the cell set and edges, triangles and quadrangles
     * of the boundary of the area of all 3-dimenionnal cells. This method
     * always returns a reference to the internal array.
     *
     * @return boundary CellArrays
     */
    public CellArray[] getBoundaryCellArrays() {
        return boundaryCellArrays;
    }

    /**
     * Sets the boundary CellArrays.
     *
     * @param cellArrays new value of boundary CellArrays, this reference is
     * used internally (i.e. the array is not cloned)
     */
    public void setBoundaryCellArrays(CellArray[] cellArrays) {
        if (cellArrays.length != Cell.getNProperCellTypesUpto2D()) {
            throw new IllegalArgumentException("cellArrays.length != Cell.getNProperCellTypesUpto2D()");
        }
        for (int i = 0; i < cellArrays.length; i++) {
            CellArray cellArray = cellArrays[i];
            if (cellArray == null || cellArray.getType().getValue() != i) {
                return;
            }
            //throw new IllegalArgumentException("cellArray == null || cellArray.getType().getValue() != i");
        }
        boundaryCellArrays = cellArrays;
        timestamp = System.nanoTime();
    }

    /**
     * Returns the value of boundary CellArrays at specified index. This method
     * always returns a reference to the internal CellArray.
     *
     * @param type cell type
     *
     * @return the value of boundary CellArray of specified type
     */
    public CellArray getBoundaryCellArray(CellType type) {
        if (type.getValue() < 0 || type.getValue() >= Cell.getNProperCellTypesUpto2D()) {
            throw new IllegalArgumentException("type.getValue() < 0 || type.getValue() >= Cell.getNProperCellTypes()");
        }
        return this.boundaryCellArrays[type.getValue()];
    }

    /**
     * Sets the new boundary CellArray.
     *
     * @param newCellArray new boundary CellArray at specified index (==
     * newCellArray.getType().getValue()), this reference is used internally
     * (i.e. the CellArray is not cloned)
     */
    public void setBoundaryCellArray(CellArray newCellArray) {
        if (newCellArray.getType().getValue() < 0 || newCellArray.getType().getValue() >= Cell.getNProperCellTypesUpto2D()) {
            throw new IllegalArgumentException("newCellArray.getType().getValue() < 0 || newCellArray.getType().getValue() >= Cell.getNProperCellTypesUpto2D()");
        }

        boundaryCellArrays[newCellArray.getType().getValue()] = newCellArray;
        timestamp = System.nanoTime();
    }

    /**
     * Returns selection flag. A cell set can be selected or deselected by thr
     * user.
     *
     * @return true, if this cell set is selected, false otherwise
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * Sets selection flag.
     *
     * @param selected new selection flag
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
        timestamp = System.nanoTime();
    }

    /**
     * Inverts selection flag.
     */
    public void invertSelected() {
        selected = !selected;
        timestamp = System.nanoTime();
    }

    /**
     * Sets the cell data component at specified index.
     *
     * @param i index
     * @param data0 the new cell data component
     * @param name the name of new component
     */
    public void setComponent(int i, LargeArray data0, String name) {
        if (data0.length() % nCells != 0 || i < 0) {
            throw new IllegalArgumentException("data0.length() % nCells != 0 || i < 0");
        }
        if (i >= components.size()) {
            components.add(DataArray.create(data0, (int) (data0.length() / nCells), name));
        } else {
            components.set(i, DataArray.create(data0, (int) (data0.length() / nCells), name));
        }
        schema.setComponentSchemasFromData(components);
        timestamp = System.nanoTime();
    }

    /**
     * Returns true if a given CellSet is data compatible with this CellSet,
     * false otherwise
     *
     * @param f input CellSet
     *
     * @return true if a given CellSet is data compatible with this CellSet,
     * false otherwise
     */
    public boolean isCompatibleWith(CellSet f) {
        return this.schema.isCompatibleWith(f.getSchema());
    }

    private static int shortHash(int k, int cellNodes, int[] nodes) {
        short seed = 13131; // 31 131 1313 13131 131313 etc..
        short h = 0;

        for (int i = 0; i < cellNodes; i++) {
            h = (short) (h * seed + nodes[cellNodes * k + i]);
        }
        return (int) h - Short.MIN_VALUE;
    }

    /**
     * Merges cell array with own cell array of corresponding type removing
     * duplicates.
     *
     * @param ca merged array
     * @param mca array of cell arrays - ca is merged to element of mca of
     * corresponding type
     */
    private static void mergeCellArray(CellArray ca, CellArray[] mca) {
        if (ca == null) {
            return;
        }
        CellType tp = ca.getType();
        int nv = tp.getNVertices();
        if (mca[tp.getValue()] == null) {
            ca.cancelDuplicates();
            mca[tp.getValue()] = ca;
            return;
        }
        CellArray ca0 = mca[tp.getValue()];
        int nc0 = ca0.getNCells();
        int nc = ca.getNCells();
        int[] nds0 = ca0.getNodes();
        int[] nds = ca.getNodes();
        byte[] or0 = ca0.getOrientations();
        byte[] or = ca.getOrientations();
        int startBinSize = 2;
        int[][] indexBins = new int[65536][startBinSize];
        int[] indexN = new int[65536];
        int n = nc + nc0;
        for (int i = 0; i < indexN.length; i++) {
            indexN[i] = 0;
        }
        for (int i = 0; i < nc0; i++) {
            int h = shortHash(i, nv, nds0);
            int k = -1;
            s1:
            for (int j = 0; j < indexN[h]; j++) {
                int l = indexBins[h][j];
                for (int m = 0; m < nv; m++) {
                    if (nds0[nv * i + m] != nds0[nv * l + m]) {
                        continue s1; // cell i is not a duplicate of cell j in bin h
                    }
                }
                k = j; // all nodes match - duplicate found
                break;
            }
            if (k != -1) // duplicate found - removing
            {
                for (int j = k + 1; j < indexN[h]; j++) {
                    indexBins[h][j - 1] = indexBins[h][j];
                }
                indexN[h] -= 1;
                n -= 2; // two cells removed
            } else {
                if (indexN[h] + 1 >= indexBins[h].length) // increase bin capacity
                {
                    int[] t = new int[2 * indexBins[h].length];
                    System.arraycopy(indexBins[h], 0, t, 0, indexBins[h].length);
                    indexBins[h] = t;
                }
                indexBins[h][indexN[h]] = i;
                indexN[h] += 1; // cell index added to bin
            }
        }

        for (int i = 0; i < nc; i++) {
            int h = shortHash(i, nv, nds);
            int k = -1;
            s2:
            for (int j = 0; j < indexN[h]; j++) {
                int l = indexBins[h][j];
                if (l < nc0) {
                    for (int m = 0; m < nv; m++) {
                        if (nds[nv * i + m] != nds0[nv * l + m]) {
                            continue s2; // cell i is not a duplicate of cell j in bin h
                        }
                    }
                } else {
                    l -= nc0;
                    for (int m = 0; m < nv; m++) {
                        if (nds[nv * i + m] != nds[nv * l + m]) {
                            continue s2; // cell i is not a duplicate of cell j in bin h
                        }
                    }
                }
                k = j; // all nodes match - duplicate found
                break;
            }
            if (k != -1) // duplicate found - removing
            {
                for (int j = k + 1; j < indexN[h]; j++) {
                    indexBins[h][j - 1] = indexBins[h][j];
                }
                indexN[h] -= 1;
                n -= 2; // two cells removed
            } else {
                if (indexN[h] + 1 >= indexBins[h].length) // increase bin capacity
                {
                    int[] t = new int[2 * indexBins[h].length];
                    System.arraycopy(indexBins[h], 0, t, 0, indexBins[h].length);
                    indexBins[h] = t;
                }
                indexBins[h][indexN[h]] = i + nc0;
                indexN[h] += 1; // cell index added to bin
            }
        }

        int maxBin = 0;
        for (int i = 0; i < indexN.length; i++) {
            if (maxBin < indexN[i]) {
                maxBin = indexN[i];
            }
        }
        int[] xNodes = new int[nv * n];
        byte[] xOrientations = new byte[n];
        if (ca0.getDataIndices() != null && ca.getDataIndices() != null) {
            int[] di0 = ca0.getDataIndices();
            int[] di = ca.getDataIndices();
            int[] xDataIndices = new int[n];
            for (int i = 0, l = 0; i < 65536; i++) {
                for (int j = 0; j < indexN[i]; j++, l++) {
                    int m = indexBins[i][j];
                    if (m < nc0) {
                        for (int k = 0; k < nv; k++) {
                            xNodes[l * nv + k] = nds0[m * nv + k];
                        }
                        xOrientations[l] = or0[m];
                        xDataIndices[l] = di0[m];
                    } else {
                        m -= nc0;
                        for (int k = 0; k < nv; k++) {
                            xNodes[l * nv + k] = nds[m * nv + k];
                        }
                        xOrientations[l] = or[m];
                        xDataIndices[l] = di[m];
                    }
                }
            }
            mca[tp.getValue()] = new CellArray(tp, xNodes, xOrientations, xDataIndices);
        } else {
            for (int i = 0, l = 0; i < 65536; i++) {
                for (int j = 0; j < indexN[i]; j++, l++) {
                    int m = indexBins[i][j];
                    if (m < nc0) {
                        for (int k = 0; k < nv; k++) {
                            xNodes[l * nv + k] = nds0[m * nv + k];
                        }
                        xOrientations[l] = or0[m];
                    } else {
                        m -= nc0;
                        for (int k = 0; k < nv; k++) {
                            xNodes[l * nv + k] = nds[m * nv + k];
                        }
                        xOrientations[l] = or[m];
                    }
                }
            }
            mca[tp.getValue()] = new CellArray(tp, xNodes, xOrientations, null);
        }

        mca[tp.getValue()].setCleanedDuplicates(true);
    }

    /**
     * Adds cells from the input cell array removing duplicates.
     *
     * @param ca input cell array
     */
    public void addCells(CellArray ca) {
        if (cellArrays[ca.getType().getValue()] == null) {
            cellArrays[ca.getType().getValue()] = ca.cloneDeep();
        } else {
            CellArray oldCa = cellArrays[ca.getType().getValue()];
            int[] nodes = new int[oldCa.getNodes().length + ca.getNodes().length];
            System.arraycopy(oldCa.getNodes(), 0, nodes, 0, oldCa.getNodes().length);
            System.arraycopy(ca.getNodes(), 0, nodes, oldCa.getNodes().length, ca.getNodes().length);
            byte[] orientations = new byte[oldCa.getNCells() + ca.getNCells()];
            System.arraycopy(oldCa.getOrientations(), 0, orientations, 0, oldCa.getNCells());
            System.arraycopy(ca.getOrientations(), 0, orientations, oldCa.getNCells(), ca.getNCells());
            if (oldCa.getDataIndices() != null && ca.getDataIndices() != null) {
                int[] dataIndices = new int[oldCa.getDataIndices().length + ca.getDataIndices().length];
                System.arraycopy(oldCa.getDataIndices(), 0, dataIndices, 0, oldCa.getDataIndices().length);
                System.arraycopy(ca.getDataIndices(), 0, dataIndices, oldCa.getDataIndices().length, ca.getDataIndices().length);
                cellArrays[ca.getType().getValue()] = new CellArray(ca.getType(), nodes, orientations, dataIndices);
            } else {
                cellArrays[ca.getType().getValue()] = new CellArray(ca.getType(), nodes, orientations, null);
            }
            cellArrays[ca.getType().getValue()].getEdges();
        }
        nCells = 0;
        for (CellArray cellArray : cellArrays) {
            if (cellArray != null) {
                nCells += cellArray.getNCells();
            }
        }
        timestamp = System.nanoTime();
    }

    /**
     * Triangulates this cell set.
     *
     * @return triangulated cell set containing only points, segments, triangles
     * and tetrahedra. All other cells are triangulated.
     */
    public CellSet getTriangulated() {
        CellSet triangulated = new CellSet(getName() + "_triangulated");
        for (CellArray cellArray : cellArrays) {
            if (cellArray != null) {
                triangulated.addCells(cellArray.getTriangulated());
            }
        }
        for (DataArray component : components) {
            triangulated.addComponent(component.cloneShallow());
        }
        return triangulated;
    }

    /**
     * Fills boundary cell arrays with the boundary cells. For each 3D cell, it
     * creates its boundary cells and removes duplicated face cells.
     */
    public void generateExternFaces() {
        // cleanup first       
        for (int i = 0; i < Cell.getNProperCellTypesUpto2D(); i++) {
            boundaryCellArrays[i] = null;
        }

        // then work       
        if (cellArrays[CellType.TETRA.getValue()] == null && cellArrays[CellType.PYRAMID.getValue()] == null
                && cellArrays[CellType.PRISM.getValue()] == null && cellArrays[CellType.HEXAHEDRON.getValue()] == null) {
            boundaryCellArrays[CellType.TRIANGLE.getValue()] = cellArrays[CellType.TRIANGLE.getValue()];
            boundaryCellArrays[CellType.QUAD.getValue()] = cellArrays[CellType.QUAD.getValue()];
            updateActiveNodes();
        } else {
            if (cellArrays[CellType.TRIANGLE.getValue()] != null) {
                boundaryCellArrays[CellType.TRIANGLE.getValue()] = cellArrays[CellType.TRIANGLE.getValue()].cloneDeep();
            }
            if (cellArrays[CellType.QUAD.getValue()] != null) {
                boundaryCellArrays[CellType.QUAD.getValue()] = cellArrays[CellType.QUAD.getValue()].cloneDeep();
            }
            for (CellArray cellArray1 : cellArrays) {
                if (cellArray1 != null && cellArray1.getType().getDim() == 3) {
                    ArrayList<CellArray> faces = cellArray1.getFaces();
                    for (CellArray cellArray : faces) {
                        mergeCellArray(cellArray, boundaryCellArrays);
                    }
                }
            }

            updateActiveNodes();
        }
        timestamp = System.nanoTime();
    }

    /**
     * Creates normal vectors for 2D boundary cells.
     *
     * @param coords coordinates of the polyhedron.
     */
    private void createBoundaryCellNormals(FloatLargeArray coords) {
        if (boundaryCellArrays == null || coords == null) {
            throw new IllegalArgumentException("boundaryCellArrays == null || coords == null");
        }
        for (int m = 0; m < Cell.getNProperCellTypesUpto2D(); m++) {
            CellArray ar = boundaryCellArrays[m];
            if (ar == null || ar.getDim() != 2 || ar.getNCells() < 1) {
                continue;
            }
            FloatLargeArray cellNormals = new FloatLargeArray(3 * (long)ar.getNCells());
            int n = ar.getType().getNVertices();
            float[] v0 = new float[3];
            float[] v1 = new float[3];
            int[] nodes = ar.getNodes();
            byte[] orientation = ar.getOrientations();
            int i;
            for (i = 0; i < ar.getNCells(); i++) {
                for (int j = 0; j < 3; j++) {
                    v0[j] = coords.getFloat(3 * (long) nodes[i * n + 1] + j) - coords.getFloat(3 * (long) nodes[i * n] + j);
                    v1[j] = coords.getFloat(3 * (long) nodes[i * n + 2] + j) - coords.getFloat(3 * (long) nodes[i * n] + j);
                }
                cellNormals.setFloat(3 * (long)i, v0[1] * v1[2] - v0[2] * v1[1]);
                cellNormals.setFloat(3 * (long)i + 1, v0[2] * v1[0] - v0[0] * v1[2]);
                cellNormals.setFloat(3 * (long)i + 2, v0[0] * v1[1] - v0[1] * v1[0]);
                float r = cellNormals.getFloat(3 * (long)i) * cellNormals.getFloat(3 * (long)i)
                        + cellNormals.getFloat(3 * (long)i + 1) * cellNormals.getFloat(3 * (long)i + 1)
                        + cellNormals.getFloat(3 * (long)i + 2) * cellNormals.getFloat(3 * (long)i + 2);
                if (r == 0) {
                    continue;
                }
                r = (float) (sqrt(r));
                if (orientation[i] == 1) {
                    for (int j = 0; j < 3; j++) {
                        cellNormals.setFloat(3 * (long)i + j, cellNormals.getFloat(3 * (long)i + j) / r);
                    }
                } else {
                    for (int j = 0; j < 3; j++) {
                        cellNormals.setFloat(3 * (long)i + j, cellNormals.getFloat(3 * (long)i + j) / -r);
                    }
                }
            }
            ar.setCellNormals(cellNormals);
        }
        timestamp = System.nanoTime();

    }

    /**
     * Generates boundary cells, cell normals and computes dihedral angles on surface.
     *
     * @param coords coordinates of the polyhedron.
     */
    public void generateDisplayData(FloatLargeArray coords) {
        generateExternFaces();
        CellArray triEdges = null, quadEdges = null, seg = cellArrays[CellType.SEGMENT.getValue()];
        createBoundaryCellNormals(coords);

        int nTriangleEdges = 0;
        int[] triEdgeNodes = null;
        int[] triEdgeFaces = null;
        FloatLargeArray triNormals = null;
        byte[] triOrientations = null;
        int[] triDataIndices = null;

        int nQuadEdges = 0;
        int[] quadEdgeNodes = null;
        int[] quadEdgeFaces = null;
        FloatLargeArray quadNormals = null;
        byte[] quadOrientations = null;
        int[] quadDataIndices = null;

        if (boundaryCellArrays[CellType.TRIANGLE.getValue()] != null) {
            triEdges = boundaryCellArrays[CellType.TRIANGLE.getValue()].getEdges();
            nTriangleEdges = triEdges.getNCells();
            triEdgeNodes = triEdges.getNodes();
            triEdgeFaces = triEdges.getFaceIndices();
            triNormals = boundaryCellArrays[CellType.TRIANGLE.getValue()].getCellNormals();
            triOrientations = triEdges.getOrientations();
            triDataIndices = triEdges.getDataIndices();
        }

        if (boundaryCellArrays[CellType.QUAD.getValue()] != null) {
            quadEdges = boundaryCellArrays[CellType.QUAD.getValue()].getEdges();
            nQuadEdges = quadEdges.getNCells();
            quadEdgeNodes = quadEdges.getNodes();
            quadEdgeFaces = quadEdges.getFaceIndices();
            quadNormals = boundaryCellArrays[CellType.QUAD.getValue()].getCellNormals();
            quadOrientations = quadEdges.getOrientations();
            quadDataIndices = quadEdges.getDataIndices();
        }

        int startBinSize = 12;
        int[][] indexBins = new int[65536][startBinSize];
        int[] indexN = new int[65536];
        int nFaceEdges = 0;
        for (int i = 0; i < indexN.length; i++) {
            indexN[i] = 0;
        }
        for (int i = 0; i < nTriangleEdges; i++) {
            int h = shortHash(i, 2, triEdgeNodes);
            int k = -1;
            int l = 0;
            for (int j = 0; j < indexN[h]; j += 2) {
                l = indexBins[h][j];
                if (triEdgeNodes[2 * i] != triEdgeNodes[2 * l] || triEdgeNodes[2 * i + 1] != triEdgeNodes[2 * l + 1]) {
                    continue; // cell i is not a duplicate of cell j in bin h
                }
                k = j; // all nodes match - duplicate found
                break;
            }
            if (k != -1) // duplicate found - removing
            {
                if (indexBins[h][k] < 0) {
                    indexBins[h][k + 1] = -18001;
                } else {
                    int i0 = triEdgeFaces[i];
                    int l0 = triEdgeFaces[l];
                    double s = 0;
                    for (int j = 0; j < 3; j++) {
                        s += triNormals.getFloat(3 * (long)i0 + j) * triNormals.getFloat(3 * (long)l0 + j);
                    }
                    indexBins[h][k + 1] = -(int) (18000 * acos(s) / PI) - 1;
                }
            } else {
                if (indexN[h] + 1 >= indexBins[h].length) // increase bin capacity
                {
                    int[] t = new int[2 * indexBins[h].length];
                    System.arraycopy(indexBins[h], 0, t, 0, indexBins[h].length);
                    indexBins[h] = t;
                }
                indexBins[h][indexN[h]] = i;
                indexBins[h][indexN[h] + 1] = triEdgeFaces[i];
                indexN[h] += 2; // cell index added to bin
                nFaceEdges += 1;
            }
        }

        for (int i = 0; i < nQuadEdges; i++) {
            int h = shortHash(i, 2, quadEdgeNodes);
            int k = -1;
            int l = 0;
            boolean triangleFound = false;
            for (int j = 0; j < indexN[h]; j += 2) {
                l = indexBins[h][j];
                if (l < nTriangleEdges) {
                    triangleFound = true;
                    if (quadEdgeNodes[2 * i] != triEdgeNodes[2 * l] || quadEdgeNodes[2 * i + 1] != triEdgeNodes[2 * l + 1]) {
                        continue; // cell i is not a duplicate of cell j in bin h
                    }
                } else {
                    triangleFound = false;
                    l -= nTriangleEdges;
                    if (quadEdgeNodes[2 * i] != quadEdgeNodes[2 * l] || quadEdgeNodes[2 * i + 1] != quadEdgeNodes[2 * l + 1]) {
                        continue; // cell i is not a duplicate of cell j in bin h
                    }
                }
                k = j; // all nodes match - duplicate found
                break;
            }
            if (k != -1) // duplicate found - removing
            {
                if (indexBins[h][k + 1] < 0) {
                    indexBins[h][k + 1] = -18001;
                } else {
                    int i0 = quadEdgeFaces[i];
                    if (triangleFound) {
                        int l0 = triEdgeFaces[l];
                        double s = 0;
                        for (int j = 0; j < 3; j++) {
                            s += quadNormals.getFloat(3 * (long)i0 + j) * triNormals.getFloat(3 * (long)l0 + j);
                        }
                        indexBins[h][k + 1] = -(int) (18000 * acos(s) / PI) - 1;
                    } else {
                        int l0 = quadEdgeFaces[l];
                        double s = 0;
                        for (int j = 0; j < 3; j++) {
                            s += quadNormals.getFloat(3 * (long)i0 + j) * quadNormals.getFloat(3 * (long)l0 + j);
                        }
                        indexBins[h][k + 1] = -(int) (18000 * acos(s) / PI) - 1;
                    }
                }
            } else {
                if (indexN[h] + 1 >= indexBins[h].length) // increase bin capacity
                {
                    int[] t = new int[2 * indexBins[h].length];
                    System.arraycopy(indexBins[h], 0, t, 0, indexBins[h].length);
                    indexBins[h] = t;
                }
                indexBins[h][indexN[h]] = i + nTriangleEdges;
                indexBins[h][indexN[h] + 1] = quadEdgeFaces[i];
                indexN[h] += 2; // cell index added to bin
                nFaceEdges += 1;
            }
        }

        int maxBin = 0;
        for (int i = 0; i < indexN.length; i++) {
            if (maxBin < indexN[i]) {
                maxBin = indexN[i];
            }
        }
        int nEdges = nFaceEdges;
        if (seg != null) {
            nEdges += seg.getNCells();
        }
        int[] xNodes = new int[2 * nEdges];
        byte[] xOrientations = new byte[nEdges];
        float[] dihedrals = new float[nEdges];
        int l = 0;

        if ((triEdges == null || triDataIndices != null)
                && (quadEdges == null || quadDataIndices != null)
                && (cellArrays[CellType.SEGMENT.getValue()] == null
                || cellArrays[CellType.SEGMENT.getValue()].getDataIndices() != null)) {
            int[] xDataIndices = new int[nEdges];
            for (int i = 0; i < 65536; i++) {
                for (int j = 0; j < indexN[i]; j += 2, l++) {
                    int m = indexBins[i][j];
                    if (m < nTriangleEdges) {
                        for (int k = 0; k < 2; k++) {
                            xNodes[l * 2 + k] = triEdgeNodes[m * 2 + k];
                        }
                        xOrientations[l] = 1;
                        xDataIndices[l] = triDataIndices[m];
                    } else {
                        m -= nTriangleEdges;
                        for (int k = 0; k < 2; k++) {
                            xNodes[l * 2 + k] = quadEdgeNodes[m * 2 + k];
                        }
                        xOrientations[l] = quadOrientations[m];
                        xDataIndices[l] = quadDataIndices[m];
                    }
                    if (indexBins[i][j + 1] < 0) {
                        dihedrals[l] = (-indexBins[i][j + 1] - 1) / 100.f;
                    } else {
                        dihedrals[l] = 181.f;
                    }
                }
            }
            if (seg != null) {
                int nSeg = seg.getNCells();
                int[] segNodes = seg.getNodes();
                byte[] segOrientations = seg.getOrientations();
                int[] segDataIndices = seg.getDataIndices();
                for (int i = 0; i < nSeg; i++, l++) {
                    for (int k = 0; k < 2; k++) {
                        xNodes[l * 2 + k] = segNodes[2 * i + k];
                    }
                    xOrientations[l] = segOrientations[i];
                    xDataIndices[l] = segDataIndices[i];
                    dihedrals[l] = 181.f;
                }
            }
            boundaryCellArrays[CellType.SEGMENT.getValue()] = new CellArray(CellType.SEGMENT, xNodes, xOrientations, xDataIndices);
            boundaryCellArrays[CellType.SEGMENT.getValue()].setCellDihedrals(dihedrals);
        } else {
            for (int i = 0; i < 65536; i++) {
                for (int j = 0; j < indexN[i]; j += 2, l++) {
                    int m = indexBins[i][j];
                    if (m < nTriangleEdges) {
                        for (int k = 0; k < 2; k++) {
                            xNodes[l * 2 + k] = triEdgeNodes[m * 2 + k];
                        }
                        xOrientations[l] = triOrientations[m];
                    } else {
                        m -= nTriangleEdges;
                        for (int k = 0; k < 2; k++) {
                            xNodes[l * 2 + k] = quadEdgeNodes[m * 2 + k];
                        }
                        xOrientations[l] = quadOrientations[m];
                    }
                    dihedrals[l] = (-indexBins[i][j + 1] - 1) / 100.f;
                }
            }
            if (seg != null) {
                int nSeg = seg.getNCells();
                int[] segNodes = seg.getNodes();
                byte[] segOrientations = seg.getOrientations();
                for (int i = 0; i < nSeg; i++, l++) {
                    for (int k = 0; k < 2; k++) {
                        xNodes[l * 2 + k] = segNodes[2 * i + k];
                    }
                    xOrientations[l] = segOrientations[i];
                    dihedrals[l] = 180.f;
                }
            }
            boundaryCellArrays[CellType.SEGMENT.getValue()] = new CellArray(CellType.SEGMENT, xNodes, xOrientations, null);
            boundaryCellArrays[CellType.SEGMENT.getValue()].setCellDihedrals(dihedrals);
        }
        timestamp = System.nanoTime();
    }

    /**
     * Returns true if a given CellSet has a structure compatible with this
     * CellSet, false otherwise.
     *
     * @param s input CellSet
     *
     * @return true if a given CellSet has a structure compatible with this
     * CellSet, false otherwise
     */
    public boolean isStructCompatibleWith(CellSet s) {
        for (int i = 0; i < cellArrays.length; i++) {
            if (cellArrays[i] == null) {
                if (s.getCellArray(CellType.getType(i)) != null) {
                    return false;
                }
                continue;
            }
            if (s.getCellArray(CellType.getType(i)) == null
                    || !cellArrays[i].isStructCompatible(s.getCellArray(CellType.getType(i)))) {
                return false;
            }
        }
        return true;
    }

    private void updateActiveNodes() {
        int maxNode = -1;
        for (CellArray cellArray : cellArrays) {
            if (cellArray != null && cellArray.getNodes() != null) {
                int[] nodes = cellArray.getNodes();
                for (int j = 0; j < nodes.length; j++) {
                    if (nodes[j] > maxNode) {
                        maxNode = nodes[j];
                    }
                }
            }
        }
        for (CellArray cellArray : boundaryCellArrays) {
            if (cellArray != null && cellArray.getNodes() != null) {
                int[] nodes = cellArray.getNodes();
                for (int j = 0; j < nodes.length; j++) {
                    if (nodes[j] > maxNode) {
                        maxNode = nodes[j];
                    }
                }
            }
        }
        boolean[] activeNodes = new boolean[maxNode + 1];
        for (int i = 0; i < activeNodes.length; i++) {
            activeNodes[i] = false;
        }
        nActiveNodes = 0;
        for (CellArray cellArray : cellArrays) {
            if (cellArray != null && cellArray.getNodes() != null) {
                int[] nodes = cellArray.getNodes();
                for (int j = 0; j < nodes.length; j++) {
                    if (!activeNodes[nodes[j]]) {
                        activeNodes[nodes[j]] = true;
                        nActiveNodes += 1;
                    }
                }
            }
        }
        for (CellArray cellArray : boundaryCellArrays) {
            if (cellArray != null && cellArray.getNodes() != null) {
                int[] nodes = cellArray.getNodes();
                for (int j = 0; j < nodes.length; j++) {
                    if (!activeNodes[nodes[j]]) {
                        activeNodes[nodes[j]] = true;
                        nActiveNodes += 1;
                    }
                }
            }
        }
        int[] nodes = new int[nActiveNodes];
        byte[] orientations = new byte[nActiveNodes];
        for (int i = 0, j = 0; i < activeNodes.length; i++) {
            if (activeNodes[i]) {
                nodes[j] = i;
                j += 1;
            }
        }
        CellArray bPCA = new CellArray(CellType.POINT, nodes, orientations, null);
        boundaryCellArrays[CellType.POINT.getValue()] = bPCA;
        timestamp = System.nanoTime();
    }

    /**
     * Returns the number of nodes occurring in this cell set.
     *
     * @return the number of nodes occurring in this cell set
     */
    public int getNActiveNodes() {
        if (nActiveNodes < 0) {
            updateActiveNodes();
        }
        return nActiveNodes;
    }

    @Override
    public long getNElements() {
        if (components == null || components.isEmpty()) {
            return 0;
        }
        return components.get(0).getNElements();
    }

    /**
     * Computes cell centers and radii for all cell arrays in this cell set.
     *
     * @param coords coordinates
     */
    public void addGeometryData(FloatLargeArray coords) {
        for (CellArray ca : cellArrays) {
            if (ca != null) {
                ca.computeCellGeometryData(coords);
            }
        }
        timestamp = System.nanoTime();
    }

    /**
     * Computes normal vectors for 2D cells.
     *
     * @param coords coordinates
     */
    public void computeCellNormals(FloatLargeArray coords) {
        for (CellArray ca : cellArrays) {
            if (ca != null && ca.getDim() == 2) {
                ca.computeCellGeometryData(coords);
            }
        }
        timestamp = System.nanoTime();
    }

 
    /**
     * Returns the measure (length, area, volume) of a polyhedron described by
     * this cell set.
     *
     * @param dim dimension of the measure
     * @param coords coordinates
     *
     * @return the measure of this cell array
     */
    public float getMeasure(int dim, FloatLargeArray coords) {
        float r = 0;
        switch (dim) {
            case 0:
                if (cellArrays[CellType.POINT.getValue()] != null) {
                    r += (float) (cellArrays[CellType.POINT.getValue()].getNCells());
                }
                break;
            case 1:
                if (cellArrays[CellType.SEGMENT.getValue()] != null) {
                    r += cellArrays[CellType.SEGMENT.getValue()].getMeasure(coords);
                }
                break;
            case 2:
                if (cellArrays[CellType.TRIANGLE.getValue()] != null) {
                    r += cellArrays[CellType.TRIANGLE.getValue()].getMeasure(coords);
                }
                if (cellArrays[CellType.QUAD.getValue()] != null) {
                    r += cellArrays[CellType.QUAD.getValue()].getMeasure(coords);
                }
                break;
            case 3:
                if (cellArrays[CellType.TETRA.getValue()] != null) {
                    r += cellArrays[CellType.TETRA.getValue()].getMeasure(coords);
                }
                if (cellArrays[CellType.PYRAMID.getValue()] != null) {
                    r += cellArrays[CellType.PYRAMID.getValue()].getMeasure(coords);
                }
                if (cellArrays[CellType.PRISM.getValue()] != null) {
                    r += cellArrays[CellType.PRISM.getValue()].getMeasure(coords);
                }
                if (cellArrays[CellType.HEXAHEDRON.getValue()] != null) {
                    r += cellArrays[CellType.HEXAHEDRON.getValue()].getMeasure(coords);
                }
                break;
            default:
                break;
        }
        return r;
    }

    /**
     * Returns true if a given CellSet is fully compatible with this CellSet,
     * false otherwise.
     *
     * @param s CellSet
     *
     * @param checkComponentNames if true, then the component names are compared
     *
     * @return true if a given CellSet is fully compatible with this CellSet,
     * false otherwise
     */
    public boolean isFullyCompatibleWith(CellSet s, boolean checkComponentNames) {
        if (getNComponents() != s.getNComponents()) {
            return false;
        }
        for (int i = 0; i < getNComponents(); i++) {
            if (!getComponent(i).isFullyCompatibleWith(s.getComponent(i), checkComponentNames)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns true if this CellSet has cells of a specified type, false
     * otherwise.
     *
     * @param type cell type
     *
     * @return true if this CellSet has cells of a specified type, false
     * otherwise
     */
    public boolean hasCellsType(CellType type) {
        if (type.getValue() < 0 || type.getValue() >= Cell.getNProperCellTypes()) {
            return false;
        }

        if (getCellArray(type) != null) {
            return true;
        }

        CellArray[] bndrs = getBoundaryCellArrays();
        if (bndrs != null) {
            for (CellArray bndr : bndrs) {
                if (bndr != null && bndr.getType() == type) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns true if this CellSet has cells of type POINT, false otherwise.
     *
     * @return true if this CellSet has cells of type POINT, false otherwise
     */
    public boolean hasCellsPoint() {
        return hasCellsType(CellType.POINT);
    }

    /**
     * Returns true if this CellSet has cells of type SEGMENT, false otherwise.
     *
     * @return true if this CellSet has cells of type SEGMENT, false otherwise
     */
    public boolean hasCellsSegment() {
        return hasCellsType(CellType.SEGMENT);
    }

    /**
     * Returns true if this CellSet has cells of type TRIANGLE, false otherwise.
     *
     * @return true if this CellSet has cells of type TRIANGLE, false otherwise
     */
    public boolean hasCellsTriangle() {
        return hasCellsType(CellType.TRIANGLE);
    }

    /**
     * Returns true if this CellSet has cells of type QUAD, false otherwise.
     *
     * @return true if this CellSet has cells of type QUAD, false otherwise
     */
    public boolean hasCellsQuad() {
        return hasCellsType(CellType.QUAD);
    }

    /**
     * Returns true if this CellSet has cells of type TETRA, false otherwise.
     *
     * @return true if this CellSet has cells of type TETRA, false otherwise
     */
    public boolean hasCellsTetra() {
        return hasCellsType(CellType.TETRA);
    }

    /**
     * Returns true if this CellSet has cells of type PYRAMID, false otherwise.
     *
     * @return true if this CellSet has cells of type PYRAMID, false otherwise
     */
    public boolean hasCellsPyramid() {
        return hasCellsType(CellType.PYRAMID);
    }

    /**
     * Returns true if this CellSet has cells of type PRISM, false otherwise.
     *
     * @return true if this CellSet has cells of type PRISM, false otherwise
     */
    public boolean hasCellsPrism() {
        return hasCellsType(CellType.PRISM);
    }

    /**
     * Returns true if this CellSet has cells of type HEXAHEDRON, false
     * otherwise.
     *
     * @return true if this CellSet has cells of type HEXAHEDRON, false
     * otherwise
     */
    public boolean hasCellsHexahedron() {
        return hasCellsType(CellType.HEXAHEDRON);
    }

    /**
     * Returns true if this CellSet has 1D cells, false otherwise.
     *
     * @return true if this CellSet has 1D cells, false otherwise.
     */
    public boolean hasCells1D() {
        return (hasCellsSegment());
    }

    /**
     * Returns true if this CellSet has 2D cells, false otherwise.
     *
     * @return true if this CellSet has 2D cells, false otherwise.
     */
    public boolean hasCells2D() {
        return (hasCellsTriangle() || hasCellsQuad());
    }

    /**
     * Returns true if this CellSet has 3D cells, false otherwise.
     *
     * @return true if this CellSet has 3D cells, false otherwise.
     */
    public boolean hasCells3D() {
        return (hasCellsTetra() || hasCellsPyramid() || hasCellsPrism() || hasCellsHexahedron());
    }

    /**
     * Returns 3 if this CellSet has 3D cells, 2 if this CellSet has 2D cells
     * and not 3D cells, 1, if this CellSet has only 1D cells, 0 otherwise.
     *
     * @return 3 if this CellSet has 3D cells, 2 if this CellSet has 2D cells
     * and not 3D cells, 1, if this CellSet has only 1D cells, 0 otherwise.
     */
    public int getNCellDims() {
        if (hasCells3D()) {
            return 3;
        } else if (hasCells2D()) {
            return 2;
        } else if (hasCells1D()) {
            return 1;
        } else {
            return 0;
        }
    }

}
